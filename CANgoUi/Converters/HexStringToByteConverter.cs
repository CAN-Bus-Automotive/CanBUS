﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace CANgoUi.Converters
{
    public class HexStringToByteConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            byte? intValue = value as byte?;

            var result = intValue?.ToString("X");

            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var hexString = value as string;

            if (string.IsNullOrWhiteSpace(hexString))
                return 0;

            var clearHexString = hexString.Replace(" ", string.Empty);

            byte? result = System.Convert.ToByte(clearHexString, 16);

            return result;
        }
    }
}
