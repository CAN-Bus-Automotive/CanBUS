﻿using System.Windows;

namespace CANgoUi.Models.DataTypes
{
    public class UserSettings
    {
        // Window settings
        public int WindowWith { get; set; } = 1224;
        public int WindowHeight { get; set; } = 575;
        public int LeftPosition { get; set; } = 300;
        public int TopPosition { get; set; } = 300;
        public WindowState WindowState { get; set; } = WindowState.Normal;
        public bool ShowBitView { get; set; } = false;

        // Connection settings
        public string PortName { get; set; } = "COM1";
        public string Baudrate { get; set; } = "500";
        public bool AutoConnect { get; set; }

        // Buffer settings
        public bool BitViewVisibility { get; set; } = false;

        // Other settings
        public bool UseLog { get; set; }
    }
}