﻿using CANgoUi.Mappers;
using CANgoUi.Models.DataTypes;
using GalaSoft.MvvmLight;
using LawicelLib;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Threading;
using System.Threading.Tasks;
using CANFrameLib;
using PostSharp.Serialization;

namespace CANgoUi.Services
{
    public class ConnectionService : ViewModelBase
    {
        // Private
        private readonly Lawicel _lawicel;
        private readonly MessageService _messageService;
        private BackgroundWorker _worker;
        private Stopwatch _stopwatch;

        // Public
        public string SelectedPort { get; set; }
        public string SelectedBaud { get; set; }
        public string Version { get; set; }
        public ConnectionStatus ConnectionStatus { get; set; } = ConnectionStatus.OFFLINE;
        public readonly ObservableCollection<string> ValidBaudrates;
        public event Action<ConnectionStatus> ConnectionStatusChanged;
        public event Action<CANFrame> FrameReceived;

        // Constructor
        public ConnectionService(Lawicel lawicel, MessageService messageService)
        {
            ValidBaudrates = new ObservableCollection<string>
            {
                "33.333",
                "83.333",
                "50",
                "100",
                "125",
                "250",
                "500",
                "750",
                "1000",
            };
            _stopwatch = new Stopwatch();
            _stopwatch.Start();

            _lawicel = lawicel;
            _messageService = messageService;
        }

        /// <summary>
        /// Connect to device and get all info about it
        /// </summary>
        /// <returns></returns>
        public async Task ConnectAsync()
        {
            try
            {
                Version = "Querying device...";

                /* Connect to serial port */
                _lawicel.Open(SelectedPort);

                /* Get info from device and setup it */
                await _lawicel.CloseChanel();
                Version = await _lawicel.GetVersion();
                await _lawicel.SetTimestamp(true);
                await _lawicel.SetDefaultBaudrate(ConvertToLawicelBaudFormat(SelectedBaud));
                await _lawicel.OpenChanel();
                ConnectionStatus = ConnectionStatus.ONLINE;

                _worker = new BackgroundWorker();
                _worker.WorkerSupportsCancellation = true;
                _worker.DoWork += Worker_DoWorkAsync;
                _worker.RunWorkerAsync();

            }
            catch (LawicelException e)
            {
                // TODO maybe replace this by Disconnect method
                Disconnect();
                _messageService.ShowError(e.Message);
            }

            ConnectionStatusChanged?.Invoke(ConnectionStatus);
        }
       
        /// <summary>
        /// Handle incoming CAN frame
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Worker_DoWorkAsync(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker bw = sender as BackgroundWorker;

            while (!bw.CancellationPending)
            {
                var answer = _lawicel.ReadFrame();                
                if (answer != null)
                {      
                    var localTimeStamp = _stopwatch.ElapsedMilliseconds;

                    var frame = answer.MapToCANFrame();
                    frame.SystemTimeStamp = localTimeStamp;

                    FrameReceived?.Invoke(frame);
                }
                else
                {
                    Thread.Sleep(20);
                }
            }
        }

        /// <summary>
        /// Disconnet from device and port
        /// </summary>
        public void Disconnect()
        {
            try
            {
                _worker?.CancelAsync();
                /* in blocking mode */
                _lawicel.CloseChanel().Wait(250);
            }
            catch (LawicelException e)
            {
                _messageService.ShowError(e.Message);
            }
            finally
            {
                _lawicel.Close();
                Version = string.Empty;
                ConnectionStatus = ConnectionStatus.OFFLINE;
                ConnectionStatusChanged?.Invoke(ConnectionStatus);
            }
        }


        /// <summary>
        /// Convert baud value to Lawicel format
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private string ConvertToLawicelBaudFormat(string value)
        {
            var result = string.Empty;

            switch (value)
            {
                case "33.333":
                    result = "0";
                    break;
                case "83.333":
                    result = "1";
                    break;
                case "50":
                    result = "2";
                    break;
                case "100":
                    result = "3";
                    break;
                case "125":
                    result = "4";
                    break;
                case "250":
                    result = "5";
                    break;
                case "500":
                    result = "6";
                    break;
                case "750":
                    result = "7";
                    break;
                case "1000":
                    result = "8";
                    break;
            }

            return result;
        }
    }
}