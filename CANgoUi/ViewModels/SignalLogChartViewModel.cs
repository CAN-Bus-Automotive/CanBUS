using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CANgoUi.Models.DataTypes.Dbf.Impl;
using GalaSoft.MvvmLight;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;

namespace CANgoUi.ViewModels
{
    public class SignalLogChartViewModel : ViewModelBase
    {
        // Private
        private List<OxyColor> SetColors { get; set; }
        private LinearAxis _yAxis;
        private LinearAxis _xAxis;

        // Public
        public PlotModel ChartModel { get; set; }
        public string Title { get; set; }
        public Signal SelectedSignal { get; set; }

        // Commands

        // Constructor
        public SignalLogChartViewModel(Signal signal)
        {
            ChartModel = new PlotModel();
            SelectedSignal = signal;

            SetColors = new List<OxyColor>
            {
                OxyColors.Red,
                OxyColors.Orange,
                OxyColors.Yellow,
                OxyColors.Green,
                OxyColors.Blue,
                OxyColors.Indigo,
                OxyColors.Violet,
                OxyColors.Azure
            };

            ViewInit();

            Title = $"Chart for Signal: {SelectedSignal.Name}";

            MakeChart();
        }

        private void ViewInit()
        {
            ChartModel.LegendTitle = "Legend";
            ChartModel.LegendOrientation = LegendOrientation.Vertical;
            ChartModel.LegendPlacement = LegendPlacement.Inside;
            ChartModel.LegendPosition = LegendPosition.TopRight;
            ChartModel.LegendBackground = OxyColor.FromAColor(200, OxyColors.White);
            ChartModel.LegendBorder = OxyColors.Black;
            ChartModel.PlotAreaBorderColor = OxyColors.LightGray;

            _yAxis = new LinearAxis()
            {
                ExtraGridlineColor = OxyColors.LightGray,
                TextColor = OxyColors.LightGray,
                MajorGridlineColor = OxyColors.DarkSlateGray,
                MinorGridlineColor = OxyColors.DarkSlateGray,
                MajorGridlineStyle = LineStyle.Dot,
                MajorGridlineThickness = 1,
                Title = "Value",
                TitleColor = OxyColors.LightGray,
                Position = AxisPosition.Left,
                LabelFormatter = (d) => $"0x{Convert.ToInt32(d):X}",
            };
            _xAxis = new LinearAxis()
            {
                Title = "Time",
                TitleColor = OxyColors.LightGray,
                ExtraGridlineColor = OxyColors.LightGray,
                TextColor = OxyColors.LightGray,
                MajorGridlineColor = OxyColors.DarkSlateGray,
                MinorGridlineColor = OxyColors.DarkSlateGray,
                MajorGridlineStyle = LineStyle.Dot,
                MajorGridlineThickness = 1,
                Position = AxisPosition.Bottom
            };
            ChartModel.Axes.Add(_yAxis);
            ChartModel.Axes.Add(_xAxis);
        }

        private async void MakeChart()
        {
            Random randomForColor = new Random();

            LineSeries a = new LineSeries()
            {
                Title = $"{SelectedSignal.Name}", StrokeThickness = 2,
                Color = SetColors[randomForColor.Next(0, SetColors.Count)]
            };

            await Task.Run(() =>
            {
                int timeLine = 0;
                ChartModel.Series.Add(a);
                foreach (var v in SelectedSignal.ValueLog)
                {
                    a.Points.Add(new DataPoint(timeLine, Convert.ToDouble(v)));
                    timeLine++;
                }

                _xAxis.Maximum = timeLine;
            });
        }
    }
}