using System.ComponentModel;

namespace CANgoUi.Models.DataTypes
{
    public enum Endianness
    {
        /// <summary>
        /// Little endian - least significant byte first
        /// </summary>
        [Description("Little Endian (Intel)")]
        LittleEndian,

        /// <summary>
        /// Big endian - most significant byte first
        /// </summary>
        [Description("Big Endian (Motorola)")]
        BigEndian
    }
}