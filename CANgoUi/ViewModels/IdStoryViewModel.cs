﻿using System.Collections.Generic;
using CANFrameLib;
using CANgoUi.Services;
using GalaSoft.MvvmLight;

namespace CANgoUi.ViewModels
{
    public class IdStoryViewModel : ViewModelBase
    {
        // Private
        private readonly FrameBufferService _bufferService;

        // Public
        public List<CANFrame> Frames { get; set; }
        public string Title { get; set; }

        // Commands


        // Constructor
        public IdStoryViewModel(FrameBufferService bufferService)
        {
            _bufferService = bufferService;

            Frames = _bufferService.FilterById(_bufferService.SelectedItem.Value.Id);
            Title = $"Story for Id: {_bufferService.SelectedItem.Value.Id}";
        }
    }
}