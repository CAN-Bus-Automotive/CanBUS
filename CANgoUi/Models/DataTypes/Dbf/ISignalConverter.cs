﻿using System;
using CANgoUi.Models.DataTypes.Dbf.Impl;

namespace CANgoUi.Models.DataTypes.Dbf
{
    public abstract class ISignalConverter
    {
       public string Name { get; set; }
       public abstract string Convert(Signal signal);
       public abstract Func<Signal, string> Convert();
    }
}