﻿namespace LawicelLib
{
    public class LawicelProgress
    {
        /// <summary>
        /// Total number of bytes
        /// </summary>
        public readonly int BytesTotal;

        /// <summary>
        /// Number of bytes processed
        /// </summary>
        public readonly int BytesProcessed;

        public LawicelProgress(int bytesProcessed, int bytesTotal)
        {
            BytesProcessed = bytesProcessed;
            BytesTotal = bytesTotal;
        }
    }
}
