using System;
using System.Collections;
using CANFrameLib;
using CANgoUi.Models.DataTypes;
using CANgoUi.Models.DataTypes.Dbf;
using CANgoUi.Models.DataTypes.Dbf.Impl;
using NUnit.Framework;

namespace UiTests
{
    [TestFixture]
    public class SignalTests
    {
        private Signal _signal;
        private CANFrame _canFrame;
        private CANFrame _canFrame1;

        [SetUp]
        public void SetUp()
        {
            _signal = new Signal();
            _signal.SignalType = SignalType.Int32;
            _signal.Name = "TestSignal";
            _signal.Factor = 1;
            _signal.Offset = 0;
            _signal.ByteIndex = 2;
            _signal.StartBit = 0;
            _signal.BitCount = 16;
            _signal.Endianness = Endianness.BigEndian;

            _canFrame1 = new CANFrame
            {
                Id = 0x611,
                Dcl = 8,
                Data = new byte[8] {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x73, 0x89},
                Direction = CanFrameDirections.RX,
                TimeStamp = 0x3333
            };

            _canFrame = new CANFrame
            {
                Id = 0x111,
                Dcl = 8,
                Data = new byte[8] {0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77},
                Direction = CanFrameDirections.RX,
                TimeStamp = 0x3333
            };
        }

        [Test]
        public void GetSelectedBits_ReturnValidValues()
        {
            // Arr
            var dataBits = new BitArray(_canFrame.Data);
            var expResult = new BitArray(new byte[] {0x22, 0x33});

            // Act
            var actResult = _signal.GetSelectedBits(dataBits);

            // Assert
            Assert.That(actResult, Is.EquivalentTo(expResult));
        }


        [Test]
        public void Update_BigEndian_16Bits_ValidResultInValue()
        {
            // Arr                       
            _signal.Factor = 1;
            _signal.Offset = 0;
            _signal.ByteIndex = 2;
            _signal.StartBit = 0;
            _signal.BitCount = 16;
            _signal.Endianness = Endianness.BigEndian;

            // Act
            _signal.Update(_canFrame);

            // Assert
            Assert.That(_signal.Value, Is.EqualTo(Convert.ToDouble(0x2233)));
        }

        [Test]
        public void Update_BigEndian_24Bits_ValidResultInValue()
        {
            // Arr
            var dataBits = new BitArray(_canFrame1.Data);
            var expResult = new BitArray(new byte[] {0x00, 0x73, 0x89});

            _signal.Factor = 1;
            _signal.Offset = 0;
            _signal.ByteIndex = 5;
            _signal.StartBit = 0;
            _signal.BitCount = 24;
            _signal.Endianness = Endianness.BigEndian;

            // Act
            _signal.Update(_canFrame1);

            // Assert
            Assert.That(_signal.Value, Is.EqualTo(Convert.ToDouble(0x7389)));
        }

        [Test]
        public void Update_BigEndian_32Bits_ValidResultInValue()
        {
            // Arr                       
            _signal.Factor = 1;
            _signal.Offset = 0;
            _signal.ByteIndex = 2;
            _signal.StartBit = 0;
            _signal.BitCount = 32;
            _signal.Endianness = Endianness.BigEndian;

            // Act
            _signal.Update(_canFrame);

            // Assert
            Assert.That(_signal.Value, Is.EqualTo(Convert.ToDouble(0x22334455)));
        }
        
        [Test]
        public void Update_BigEndian_64Bits_ValidResultInValue()
        {
            // Arr                       
            _signal.Factor = 1;
            _signal.Offset = 0;
            _signal.ByteIndex = 0;
            _signal.StartBit = 0;
            _signal.BitCount = 64;
            _signal.Endianness = Endianness.BigEndian;

            // Act
            _signal.Update(_canFrame);

            // Assert
            Assert.That(_signal.Value, Is.EqualTo(Convert.ToDouble(0x0011223344556677 * 1.0 + 0)));
        }

        [Test]
        public void Update_LittleEndian_16Bits_ValidResultInValue()
        {
            // Arr
            var dataBits = new BitArray(_canFrame.Data);
            var expResult = new BitArray(new byte[] {0x22, 0x33});

            _signal.Factor = 1;
            _signal.Offset = 0;
            _signal.ByteIndex = 2;
            _signal.StartBit = 0;
            _signal.BitCount = 16;
            _signal.Endianness = Endianness.LittleEndian;

            // Act
            _signal.Update(_canFrame);

            // Assert
            Assert.That(_signal.Value, Is.EqualTo(Convert.ToDouble(0x3322)));
        }
        
        [Test]
        public void Update_LittleEndian_32Bits_ValidResultInValue()
        {
            // Arr
            var dataBits = new BitArray(_canFrame.Data);
            var expResult = new BitArray(new byte[] {0x00, 0x011, 0x22, 0x33});

            _signal.Factor = 1;
            _signal.Offset = 0;
            _signal.ByteIndex = 0;
            _signal.StartBit = 0;
            _signal.BitCount = 32;
            _signal.Endianness = Endianness.LittleEndian;

            // Act
            _signal.Update(_canFrame);

            // Assert
            Assert.That(_signal.Value, Is.EqualTo(Convert.ToDouble(0x33221100)));
        }
    }
}