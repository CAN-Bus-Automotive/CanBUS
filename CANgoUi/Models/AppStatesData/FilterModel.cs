﻿using System;
using System.Collections.Generic;

namespace CANgoUi.Models.AppStatesData
{
    public class FilterModel
    {
        public bool MaskFilterMode { get; set; }
        public bool IdFilterMode { get; set; }
        public string Mask { get; set; }
        public string Code { get; set; }
        public string StartId { get; set; }
        public string EndId { get; set; }
        public bool ByIdList { get; set; } = true;
        public bool ByIdRange { get; set; }
        public string DiscreteId { get; set; }
        public List<UInt32> FilterIdList { get; set; }
    }
}
