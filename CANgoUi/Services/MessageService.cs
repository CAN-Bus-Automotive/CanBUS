﻿using System.Windows;

namespace CANgoUi.Services
{
    public class MessageService
    {
        public void ShowError(string message)
        {
            MessageBox.Show(message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        public void ShowWarning(string message)
        {
            MessageBox.Show(message, "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        public void ShowInfo(string message)
        {
            MessageBox.Show(message, "Information", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        public MessageBoxResult ShowDialog(string message)
        {
            return MessageBox.Show(message, "Dialog", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.Cancel);
        }
    }
}
