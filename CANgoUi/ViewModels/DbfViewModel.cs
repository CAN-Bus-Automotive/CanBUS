﻿using System;
using CANgoUi.Services;
using CANgoUi.Views;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System.Windows;
using CANgoUi.Models.DataTypes.Dbf.Impl;

namespace CANgoUi.ViewModels
{
    public class DbfViewModel : ViewModelBase
    {
        // Private
        private readonly DbfService _dbfService;
        private readonly MessageService _messageService;

        // Public
        public DbfMessage SelectedMessage { get; set; }
        public Signal SelectedSignal { get; set; }
        public string WindowTitle { get; set; }  = String.Empty;
        public string ActiveDbfName { get; set; }

        // Commands
        public RelayCommand AddNewMessageCmd => new RelayCommand(AddNewMessage);
        public RelayCommand DeleteMessageCmd => new RelayCommand(() => _dbfService.DeleteMessage(SelectedMessage));
        public RelayCommand EditMessageCmd => new RelayCommand(EditMessage);
        public RelayCommand NewSignalCmd => new RelayCommand(AddNewSignal);
        public RelayCommand DeleteSignalCmd => new RelayCommand(DeleteSignal);
        public RelayCommand EditSignalCmd => new RelayCommand(EditSignal);
        public RelayCommand SaveDatabaseCmd => new RelayCommand(_dbfService.SaveDatabase);
        public RelayCommand<Signal> ShowStaticChartCmd => new RelayCommand<Signal>(ShowStaticChart);
        public RelayCommand<Signal> ShowOnlineChartCmd => new RelayCommand<Signal>(ShowOnlineChart);

        public RelayCommand OpenDatabaseCmd => new RelayCommand(() =>
        {
            _dbfService.OpenDatabase();
            WindowTitle = $"DBF Editor: {_dbfService.OpenedDbfFileName}";
        });

        public RelayCommand AssociateDatabaseCmd => new RelayCommand(() =>
        {
            _dbfService.AssociateDatabase();
            ActiveDbfName = _dbfService.ActiveDbfFileName;
        });

        public RelayCommand SaveAndApplyCmd => new RelayCommand(() =>
        {
            _dbfService.SaveAndApplyDatabase();
            ActiveDbfName = _dbfService.ActiveDbfFileName;
        });

        // Constructor
        public DbfViewModel(DbfService dbfService, MessageService messageService)
        {
            _dbfService = dbfService;
            _messageService = messageService;
        }

        /// <summary>
        /// Add new DBF message to database (DBF)
        /// </summary>
        public void AddNewMessage()
        {
            NewMessageView newMessage = new NewMessageView();
            newMessage.Owner = Application.Current.MainWindow;
            newMessage.WindowStartupLocation = WindowStartupLocation.CenterOwner;

            newMessage.ShowDialog();
        }

        public void EditMessage()
        {
            if (SelectedMessage != null)
            {
                EditMessageView newMessage = new EditMessageView();
                newMessage.Owner = Application.Current.MainWindow;
                newMessage.WindowStartupLocation = WindowStartupLocation.CenterOwner;

                var addVm = new EditMessageViewModel(_dbfService);
                addVm.Id = SelectedMessage.Id;
                addVm.SelectedDcl = SelectedMessage.Dcl;
                addVm.Description = SelectedMessage.Description;

                newMessage.DataContext = addVm;

                newMessage.ShowDialog();
            }
        }

        public void AddNewSignal()
        {
            if (SelectedMessage != null)
            {
                NewSignalView newSignalView = new NewSignalView();
                newSignalView.Owner = Application.Current.MainWindow;
                newSignalView.WindowStartupLocation = WindowStartupLocation.CenterOwner;

                var newSignalVm = new NewSignalViewModel(SelectedMessage);

                newSignalView.DataContext = newSignalVm;

                newSignalView.ShowDialog();
            }
        }

        public void DeleteSignal()
        {
            if (SelectedSignal != null)
            {
                SelectedMessage.Signals.Remove(SelectedSignal);
            }
        }

        public void EditSignal()
        {
            if (SelectedSignal != null)
            {
                EditSignalView editSignalView = new EditSignalView();
                editSignalView.Owner = Application.Current.MainWindow;
                editSignalView.WindowStartupLocation = WindowStartupLocation.CenterOwner;

                var editSignalVm = new EditSignalViewModel(SelectedSignal, SelectedMessage);

                editSignalView.DataContext = editSignalVm;

                editSignalView.ShowDialog();
            }
        }

        public void ShowStaticChart(Signal signal)
        {
            if (signal != null)
            {
                SignalLogChartView signalLogChartView = new SignalLogChartView();
                signalLogChartView.Owner = Application.Current.MainWindow;
                signalLogChartView.WindowStartupLocation = WindowStartupLocation.CenterOwner;

                SignalLogChartViewModel signalLogChartVM = new SignalLogChartViewModel(signal);
                signalLogChartView.DataContext = signalLogChartVM;

                signalLogChartView.Show();
            }
        }

        public void ShowOnlineChart(Signal signal)
        {
            if (signal != null)
            {
                SignalOnlineChartView onlineChartView = new SignalOnlineChartView();
                onlineChartView.Owner = Application.Current.MainWindow;
                onlineChartView.WindowStartupLocation = WindowStartupLocation.CenterOwner;

                SignalOnlineChartViewModel onlineChartVM = new SignalOnlineChartViewModel(signal);
                onlineChartView.DataContext = onlineChartVM;

                onlineChartView.Show();
            }
        }
    }
}