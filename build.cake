#tool "nuget:?package=NUnit.ConsoleRunner"

var target = Argument("target", "Default");

Task("Default")
  .IsDependentOn("Run");

Task("Nuget")
  .Does(() =>{
      NuGetRestore("CANgo.sln");
});

Task("Build")
  .IsDependentOn("Nuget")
  .Does(() =>{
  MSBuild("CANgo.sln", new MSBuildSettings {
    Configuration = "Release"
    });
});

Task("Tests")
  .IsDependentOn("Build")
  .Does(()=>{
    NUnit3("./UiTests/bin/Release/UiTests.dll");
  });

Task("Run")
  .IsDependentOn("Tests")
  .Does(()=>{
    StartProcess("./CANgoUi/bin/Release/CANgoUi.exe");
    Information("Hello World!");
  });





RunTarget(target);