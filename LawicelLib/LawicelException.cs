﻿using System;

namespace LawicelLib
{
    public class LawicelException : Exception
    {
        public LawicelException()
        {
        }

        public LawicelException(string message) : base(message)
        {
        }

        public LawicelException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected LawicelException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) : base(info, context)
        {
        }
    }
}
