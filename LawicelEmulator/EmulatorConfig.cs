﻿namespace LawicelEmulator
{
    public class EmulatorConfig
    {
        public string PortName { get; set; }
        public int UniqFramesCount { get; set; }
    }
}
