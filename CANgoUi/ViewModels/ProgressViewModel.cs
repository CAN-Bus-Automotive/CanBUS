﻿using GalaSoft.MvvmLight;
using System.Windows;

namespace CANgoUi.ViewModels
{
    public class ProgressViewModel : ViewModelBase
    {
        public Visibility ProgressVisibility { get; set; } = Visibility.Collapsed;

        public void ShowProgress()
        {
            ProgressVisibility = Visibility.Visible;
        }

        public void HideProgress()
        {
            ProgressVisibility = Visibility.Collapsed;
        }
    }
}
