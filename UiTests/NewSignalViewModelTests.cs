using CANgoUi.Models.DataTypes.Dbf;
using CANgoUi.Models.DataTypes.Dbf.Impl;
using CANgoUi.ViewModels;
using NUnit.Framework;

namespace UiTests
{
    [TestFixture]
    public class NewSignalViewModelTests
    {
        private NewSignalViewModel _signalViewModel;
        private DbfMessage _dbfMessage;

        [SetUp]
        public void SetUp()
        {
            _dbfMessage = new DbfMessage(0x111);
            _dbfMessage.Id = 0x111;
            _dbfMessage.Dcl = 4;
            _dbfMessage.Description = "Test message";

            Signal s = new Signal();
            s.SignalType = SignalType.Int32;
            s.Name = "TestSignal";
            s.Unit = "parsec";
            s.ByteIndex = 0;
            s.StartBit = 0;
            s.BitCount = 14;

            Signal s1 = new Signal();
            s.SignalType = SignalType.Int32;
            s.Name = "TestSignal1";
            s.Unit = "apm";
            s.ByteIndex = 2;
            s.StartBit = 6;
            s.BitCount = 10;

            _dbfMessage.Signals.Add(s);
            _dbfMessage.Signals.Add(s1);

            _signalViewModel = new NewSignalViewModel(_dbfMessage);
        }

        [Test]
        public void UpdateConstraintTest_ValidMaximumBitsForNewSignal()
        {
            // Arr
            _signalViewModel.ByteIndex = 0;

            // Act
            _signalViewModel.UpdateConstraints();

            // Assert
           // Assert.That(_signalViewModel.MaximumBits, Is.EqualTo(16));
        }
    }
}