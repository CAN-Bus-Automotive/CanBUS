﻿using System.Collections.ObjectModel;
using CANFrameLib;
using GalaSoft.MvvmLight;

namespace CANgoUi.Models.DataTypes.Dbf.Impl
{
    public class DbfMessage : ViewModelBase, IDbfMessage
    {
        public uint Id { get; set; }
        public int Dcl { get; set; }
        public string Description { get; set; }
        public ObservableCollection<Signal> Signals { get; set; }

        public DbfMessage()
        {
            
        }
        
        public DbfMessage(uint id)
        {
            Id = id;

            Signals = new ObservableCollection<Signal>();
        }

        public void UpdateSignals(CANFrame canFrame)
        {
            foreach (var signal in Signals)
            {
                signal.Update(canFrame);
            }
        }
    }
}