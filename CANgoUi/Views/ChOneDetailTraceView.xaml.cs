﻿using System.Windows;
using System.Windows.Controls;

namespace CANgoUi.Views
{
    public partial class ChOneDetailTraceView : UserControl
    {
        public ChOneDetailTraceView()
        {
            InitializeComponent();
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            btn?.Command.Execute(btn.CommandParameter);
            
            //ReplayBufferList.ScrollIntoView(ReplayBufferList.SelectedItem);
        }
    }
}
