﻿using System.Text;
using System.Windows.Media;

namespace CANFrameLib
{
    public class CANFrame
    {
        public uint Id { get; set; }
        public int Dcl { get; set; }
        public bool Is29Bit { get; set; }
        public byte[] Data { get; set; }
        public CanFrameDirections Direction { get; set; }
        public uint TimeStamp { get; set; }
        public long SystemTimeStamp { get; set; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder(60);

            if (Is29Bit)
            {
                sb.AppendFormat($"{Id,-9:X8}");
            }
            else
            {
                sb.AppendFormat($"{Id,-9:X3}");
            }

            sb.AppendFormat($"{Direction,-6}");
            sb.AppendFormat($"{Dcl,-4:X}");

            for (int i = 0; i < 8; i++)
            {
                if (i < Dcl)
                {
                    sb.AppendFormat($"{Data[i],3:X2}");
                }
                else
                {
                    sb.AppendFormat("   ");
                }
            }

            sb.AppendFormat($"{SystemTimeStamp,9}");

            return sb.ToString();
        }
    }
}