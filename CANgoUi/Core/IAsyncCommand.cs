﻿using System.Threading.Tasks;
using System.Windows.Input;

namespace CANgoUi.Core
{
    public interface IAsyncCommand : ICommand
    {
        Task ExecuteAsync();
        bool CanExecute();
    }
}