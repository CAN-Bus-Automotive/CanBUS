using System;
using System.Globalization;
using System.Windows.Data;

namespace CANgoUi.Converters
{
    public class BuadToLawicelConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var result = string.Empty;

            switch ((string) value)
            {
                case "10":
                    result = "0";
                    break;
                case "20":
                    result = "1";
                    break;
                case "50":
                    result = "2";
                    break;
                case "100":
                    result = "3";
                    break;
                case "125":
                    result = "4";
                    break;
                case "250":
                    result = "5";
                    break;
                case "500":
                    result = "6";
                    break;
                case "800":
                    result = "7";
                    break;
                case "1000":
                    result = "8";
                    break;
            }

            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}