﻿using CANgoUi.Core;
using CANgoUi.Models.DataTypes;
using CANgoUi.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Threading;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using CANgoUi.Helpers;

namespace CANgoUi.ViewModels
{
    public class TransmitViewModel : ViewModelBase
    {
        // Utils
        private readonly Logger _log = NLog.LogManager.GetCurrentClassLogger();

        // Private
        private readonly TransmitService _transmitService;
        private readonly MessageService _messageService;
        private readonly FileService _fileService;

        // Public
        public TxCANFrame SelectedItem { get; set; }
        public ObservableCollection<int> ValidFrameDcl { get; set; }

        // Commands
        public RelayCommand AddNewFrameCmd => new RelayCommand(() => _transmitService.AddFrame());
        public RelayCommand UpdateDataCmd => new RelayCommand(() => _transmitService.UpdateData(SelectedItem));
        public RelayCommand DeleteAllCmd => new RelayCommand(() => _transmitService.DeleteAll());
        public RelayCommand PlayPauseCmd => new RelayCommand(() => _transmitService.PlayPause(SelectedItem));
        public RelayCommand DeleteFrameCmd => new RelayCommand(() => _transmitService.DeleteFrame(SelectedItem));
        public RelayCommand StartAllCmd => new RelayCommand(() => _transmitService.StartAllFrames());
        public RelayCommand StopAlCmd => new RelayCommand(() => _transmitService.StopAllFrames());
        public AsyncCommand OpenTxListCmd => new AsyncCommand(() => OpenTxListAsync());
        public RelayCommand SaveTxListCmd => new RelayCommand(() => SaveTxList());

        // Constructor
        public TransmitViewModel(TransmitService transmitService, MessageService messageService, FileService fileService)
        {
            _transmitService = transmitService;
            _messageService = messageService;
            _fileService = fileService;

            ValidFrameDcl = new ObservableCollection<int>() { 0, 1, 2, 3, 4, 5, 6, 7, 8 };
        }

        /// <summary>
        /// Open TX list
        /// </summary>
        /// <returns></returns>
        public async Task OpenTxListAsync()
        {
            var openFileDialog = DialogFactory.GetFileOpenDialog(FileService.TxListFileFilter);

            if (openFileDialog.ShowDialog() == true)
            {
                _transmitService.DeleteAll();
                try
                {
                    await Task.Run(async () =>
                    {
                        var txList = _fileService.OpenTxListFile(openFileDialog.FileName);
                        _log.Info(
                            $"Open TX list file: '{openFileDialog.FileName}', with frame count: {txList.Count}");

                        await DispatcherHelper.RunAsync(() =>
                         {
                             foreach (var f in txList)
                             {
                                 _transmitService.TxFrames.Add(f);
                             }
                         });
                    });
                    _log.Info($"All frames loaded in view");
                }
                catch (Exception e)
                {
                    _log.Error(e.Message);
                    _messageService.ShowError("Invalid file.");
                }
            }
        }

        /// <summary>
        /// Save TX list to file
        /// </summary>
        public void SaveTxList()
        {
            var saveFileDialog = DialogFactory.GetSaveFileDialog(FileService.TxListFileFilter);
            saveFileDialog.FileName = FileService.GeDefaultFileTxListName();

            // Stop all frames sugestion before save to file
            if (_transmitService.TxFrames.FirstOrDefault(f => f.IsRunning) != null &&
                _messageService.ShowDialog("All frames will be stopped.") ==
                MessageBoxResult.Cancel)
            {
                return;
            }

            if (saveFileDialog.ShowDialog() == true)
            {
                _transmitService.StopAllFrames();
                try
                {
                    var frm = new List<TxCANFrame>(_transmitService.TxFrames);
                    _fileService.SaveTxListFile(saveFileDialog.FileName, frm);
                    _log.Info(
                        $"Saved TX lis file: '{saveFileDialog.FileName}', with frame count: {frm.Count}");
                }
                catch (Exception e)
                {
                    _log.Error(e);
                    _messageService.ShowError(e.Message);
                }
            }
        }

    }
}
