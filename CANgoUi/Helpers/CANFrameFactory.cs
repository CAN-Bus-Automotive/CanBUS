﻿using CANFrameLib;
using CANgoUi.Models.DataTypes;

namespace CANgoUi.Helpers
{
    public class CANFrameFactory
    {
        public ObservableItem[] GetObservableItems()
        {
            var obsItems = new ObservableItem[8];
            obsItems[0] = new ObservableItem {Item = null};
            obsItems[1] = new ObservableItem {Item = null};
            obsItems[2] = new ObservableItem {Item = null};
            obsItems[3] = new ObservableItem {Item = null};
            obsItems[4] = new ObservableItem {Item = null};
            obsItems[5] = new ObservableItem {Item = null};
            obsItems[6] = new ObservableItem {Item = null};
            obsItems[7] = new ObservableItem {Item = null};

            return obsItems;
        }

        public CANFrame GetCANFrame()
        {
            CANFrame frame = new CANFrame();
            frame.Id = 0x0000;
            frame.Dcl = 0;
            frame.Direction = CanFrameDirections.TX;
            frame.Data = new byte[8] {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0};

            return frame;
        }

        public ObservableCANFrame GetObservableCANFrame()
        {
            var frame = new ObservableCANFrame();
            frame.Id = 0x0;
            frame.Dcl = 0;
            frame.Data = GetObservableItems();

            return frame;
        }

        public TxCANFrame GetTxCANFrame()
        {
            var frame = new TxCANFrame();
            frame.UseInterval = false;
            frame.Period = 100;
            frame.UseIdIncrement = false;
            frame.Frame = GetObservableCANFrame();

            return frame;
        }
    }
}