﻿using CANgoUi.Helpers;
using CANgoUi.Mappers;
using CANgoUi.Models.DataTypes;
using GalaSoft.MvvmLight;
using LawicelLib;
using System.Windows;

namespace CANgoUi.Services
{
    public class TransmitService : ViewModelBase
    {
        // Private
        private readonly Lawicel _lawicel;
        private readonly CANFrameFactory _frameFactory;

        // Public
        public ObservableRangeCollection<TxCANFrame> TxFrames { get; set; }

        // Constructor
        public TransmitService(Lawicel lawicel)
        {
            _lawicel = lawicel;

            _frameFactory = new CANFrameFactory();
            TxFrames = new ObservableRangeCollection<TxCANFrame>();
        }

        /// <summary>
        /// Add new frame to transmit list
        /// </summary>
        public void AddFrame()
        {
            var frame = _frameFactory.GetTxCANFrame();

            TxFrames.Add(frame);
        }

        /// <summary>
        /// Delete frame from transmit list
        /// </summary>
        /// <param name="frame"></param>
        public void DeleteFrame(TxCANFrame frame)
        {
            TxFrames.Remove(frame);
        }

        /// <summary>
        /// Delete all frames from transmit list
        /// </summary>
        public void DeleteAll()
        {
            TxFrames.Clear();
        }

        /// <summary>
        /// Update data in frame after Dcl changed
        /// </summary>
        public void UpdateData(TxCANFrame selectedItem)
        {
            // Reset data 
            // Very bad code!!!!
            // TODO refactor this
            if (selectedItem?.Frame?.Data != null)
            {
                for (int i = 0; i < selectedItem.Frame.Dcl; i++)
                {
                    if (selectedItem.Frame.Data[i].Item == null)
                    {
                        selectedItem.Frame.Data[i].Item = 0;
                    }
                }

                for (int i = 0; i < 8; i++)
                {
                    if (i >= selectedItem.Frame.Dcl)
                    {
                        selectedItem.Frame.Data[i].Item = null;
                    }
                }
            }
        }

        /// <summary>
        /// Play and pause selected TX frame
        /// </summary>
        /// <param name="frame"></param>
        [CheckConnection]
        public async void PlayPause(TxCANFrame frame)
        {
            if (!frame.IsRunning)
            {
                if (frame.UseInterval)
                {
                    frame.SendByPeriod();
                    frame.IsRunning = true;
                }
                else
                {
                    await _lawicel.SendTxFrame(frame.MapToByteArray());
                }
            }
            else
            {
                if (frame.UseInterval)
                {
                    frame.StopSending();
                    frame.IsRunning = false;
                }
            }
        }

        /// <summary>
        /// Stop all frames
        /// </summary>
        public void StopAllFrames()
        {
            foreach (var f in TxFrames)
            {
                if (f.IsRunning)
                {
                    if (f.UseInterval)
                    {
                        f.StopSending();
                        f.IsRunning = false;
                    }
                }
            }
        }

        /// <summary>
        /// Start all frames
        /// </summary>
        [CheckConnection]
        public void StartAllFrames()
        {
            foreach (var f in TxFrames)
            {
                if (!f.IsRunning)
                {
                    if (f.UseInterval)
                    {
                        f.SendByPeriod();
                        f.IsRunning = true;
                    }
                }
            }
        }
    }
}