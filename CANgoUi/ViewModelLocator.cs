/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:CANgoUi"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using CANgoUi.Services;
using CANgoUi.ViewModels;
using CommonServiceLocator;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using LawicelLib;

namespace CANgoUi
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
        public static void Initialize()
        {
            if (!ServiceLocator.IsLocationProviderSet)
            {
                ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);
                
                if (ViewModelBase.IsInDesignModeStatic)
                {
                    // Create design time view services and models
                }
                else
                {
                    // View models
                    SimpleIoc.Default.Register<ShellViewModel>();
                    SimpleIoc.Default.Register<ConnectionViewModel>();
                    SimpleIoc.Default.Register<MenuViewModel>();
                    SimpleIoc.Default.Register<ChOneTraceViewModel>();
                    SimpleIoc.Default.Register<ProgressViewModel>();
                    SimpleIoc.Default.Register<TransmitViewModel>();
                    SimpleIoc.Default.Register<IdStoryViewModel>();
                    SimpleIoc.Default.Register<DbfViewModel>();
                    SimpleIoc.Default.Register<AddNewMessageViewModel>();
                    SimpleIoc.Default.Register<EditMessageViewModel>();
                    SimpleIoc.Default.Register<NewSignalViewModel>();
                    SimpleIoc.Default.Register<EditSignalViewModel>();
                    SimpleIoc.Default.Register<ChartViewModel>();
                    SimpleIoc.Default.Register<ReplayBufferViewModel>();
                    SimpleIoc.Default.Register<TraceViewModel>();

                    // Services
                    SimpleIoc.Default.Register<MessageService>();
                    SimpleIoc.Default.Register<SerialPortService>();
                    SimpleIoc.Default.Register<ConnectionService>();
                    SimpleIoc.Default.Register<FrameBufferService>();
                    SimpleIoc.Default.Register<FileService>();
                    SimpleIoc.Default.Register<UserSettingsService>();
                    SimpleIoc.Default.Register<TransmitService>();
                    SimpleIoc.Default.Register<DbfService>();
                    SimpleIoc.Default.Register<TraceService>();

                    // Models
                    SimpleIoc.Default.Register<Lawicel, OneChanelLawicel>();
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
            Initialize();
        }

        public ShellViewModel Shell
        {
            get { return ServiceLocator.Current.GetInstance<ShellViewModel>(); }
        }

        public ConnectionViewModel Connection
        {
            get { return ServiceLocator.Current.GetInstance<ConnectionViewModel>(); }
        }

        public MenuViewModel Menu
        {
            get { return ServiceLocator.Current.GetInstance<MenuViewModel>(); }
        }

        public ProgressViewModel Progress
        {
            get { return ServiceLocator.Current.GetInstance<ProgressViewModel>(); }
        }

        public ChOneTraceViewModel ChOneTrace
        {
            get { return ServiceLocator.Current.GetInstance<ChOneTraceViewModel>(); }
        }

        public TransmitViewModel TransmitView
        {
            get { return ServiceLocator.Current.GetInstance<TransmitViewModel>(); }
        }

        public IdStoryViewModel IdStoryModel
        {
            get { return ServiceLocator.Current.GetInstance<IdStoryViewModel>(System.Guid.NewGuid().ToString()); }
        }

        public ReplayBufferViewModel ReplayBufferModel
        {
            get { return ServiceLocator.Current.GetInstance<ReplayBufferViewModel>(System.Guid.NewGuid().ToString()); }
        }

        public DbfViewModel DbfView
        {
            get { return ServiceLocator.Current.GetInstance<DbfViewModel>(); }
        }

        public AddNewMessageViewModel NewMessageView
        {
            get { return ServiceLocator.Current.GetInstance<AddNewMessageViewModel>(System.Guid.NewGuid().ToString()); }
        }

        public EditMessageViewModel EditMessageView
        {
            get { return ServiceLocator.Current.GetInstance<EditMessageViewModel>(System.Guid.NewGuid().ToString()); }
        }

        public NewSignalViewModel NewSignalView
        {
            get { return ServiceLocator.Current.GetInstance<NewSignalViewModel>(System.Guid.NewGuid().ToString()); }
        }

        public EditSignalViewModel EditSignalView
        {
            get { return ServiceLocator.Current.GetInstance<EditSignalViewModel>(System.Guid.NewGuid().ToString()); }
        }

        public ChartViewModel ChartView
        {
            get { return ServiceLocator.Current.GetInstance<ChartViewModel>(System.Guid.NewGuid().ToString()); }
        }

        public TraceViewModel TraceView
        {
            get { return ServiceLocator.Current.GetInstance<TraceViewModel>(); }
        }


        public ConnectionService ConnectionService
        {
            get { return ServiceLocator.Current.GetInstance<ConnectionService>(); }
        }

        public FrameBufferService BufferService
        {
            get { return ServiceLocator.Current.GetInstance<FrameBufferService>(); }
        }

        public TransmitService TxService
        {
            get { return ServiceLocator.Current.GetInstance<TransmitService>(); }
        }

        public DbfService Dbf
        {
            get { return ServiceLocator.Current.GetInstance<DbfService>(); }
        }

        public TraceService TraceService
        {
            get { return ServiceLocator.Current.GetInstance<TraceService>(); }
        }

        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
    }
}