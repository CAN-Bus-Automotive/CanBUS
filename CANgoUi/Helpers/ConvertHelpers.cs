using System.Collections;

namespace CANgoUi.Helpers
{
    public static class ConvertHelpers
    {
        public static byte[] BitArrayToByteArray(BitArray bits)
        {
            int arrLength = ((bits.Length - 1) / 8 + 1);
            byte[] ret = new byte[(bits.Length - 1) / 8 + 1];
            bits.CopyTo(ret, 0);
            return ret;
        }
        
        public static long ToBigEndian(byte[] buffer, int startIndex, int bytesToConvert)
        {
            long ret = 0;
            for (int i = 0; i < bytesToConvert; i++)
            {
                ret = unchecked((ret << 8) | buffer[startIndex + i]);
            }
            return ret;
        }
        
        public static long ToLittleEndian(byte[] buffer, int startIndex, int bytesToConvert)
        {
            long ret = 0;
            for (int i = 0; i < bytesToConvert; i++)
            {
                ret = unchecked((ret << 8) | buffer[startIndex + bytesToConvert - 1 - i]);
            }
            return ret;
        }
    }
}