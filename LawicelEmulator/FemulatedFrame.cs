﻿using System;
using System.Text;

namespace LawicelEmulator
{
    /// <summary>
    /// ONLY 11bits ID!!
    /// </summary>
    public class EmulatedFrame
    {
        public UInt32 Id { get; set; }
        public int Dcl { get; set; }
        public byte[] Data { get; set; }
        public UInt32 TimeStamp { get; set; }
        public long SystemTimeStamp { get; set; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder(60);

            sb.AppendFormat("t");

            sb.AppendFormat($"{Id:X3}");
            sb.AppendFormat($"{Dcl:X1}");
            foreach (var d in Data)
            {
                sb.AppendFormat($"{d:X2}");
            }
            sb.AppendFormat($"{TimeStamp:X4}");

            sb.AppendFormat($"\r");

            return sb.ToString();
        }
    }
}
