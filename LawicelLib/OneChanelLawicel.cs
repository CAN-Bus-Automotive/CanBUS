﻿using System;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;

namespace LawicelLib
{
    public class OneChanelLawicel : Lawicel
    {
        /// <inheritdoc />
        public override async Task OpenChanel()
        {
            _canHandleFrame = false;
            if (sp != null && sp.IsOpen)
            {
                Debug.WriteLine($"OpenChanel");
                string cmd = LawicelCmds.O + "\r";
                byte[] bytes = Encoding.ASCII.GetBytes(cmd);

                await SerialWrite(bytes, 0, bytes.Length);
                var result = await SerialRead();

                Debug.WriteLine($"Device response: {result}");
                if (result == LawicelResps.BELL.ToString())
                {
                    throw new InvalidOperationException("Invalid command.");
                }
            }

            _canHandleFrame = true;
        }

        /// <inheritdoc />
        public override async Task CloseChanel()
        {
            _canHandleFrame = false;
            if (sp != null && sp.IsOpen)
            {
                Debug.WriteLine($"CloseChanel");
                Debug.WriteLine($"DiscardInBuffer");
                sp.DiscardInBuffer();
                string cmd = LawicelCmds.C + "\r";
                byte[] bytes = Encoding.ASCII.GetBytes(cmd);

                await SerialWrite(bytes, 0, bytes.Length);
                var result = await SerialRead();

                Debug.WriteLine($"Device response: {result}");
                if (result == LawicelResps.BELL.ToString())
                {
                    throw new InvalidOperationException("Invalid command.");
                }
            }

            _canHandleFrame = true;
        }

        /// <inheritdoc />
        public override async Task SetDefaultBaudrate(string predefinedBaudrate)
        {
            Debug.WriteLine($"SetDefaultBaudrate: {predefinedBaudrate}");

            string cmd = LawicelCmds.S + predefinedBaudrate + "\r";
            byte[] bytes = Encoding.ASCII.GetBytes(cmd);

            await SerialWrite(bytes, 0, bytes.Length);
            var result = await SerialRead();

            if (result == LawicelResps.BELL.ToString())
            {
                throw new InvalidOperationException("Invalid command.");
            }
        }
    }
}