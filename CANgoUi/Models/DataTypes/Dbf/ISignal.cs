﻿using CANFrameLib;

namespace CANgoUi.Models.DataTypes.Dbf
{
    public interface ISignal
    {
        void Update(CANFrame canFrame);
        string Name { get; }
        uint ByteIndex { get; }
        uint StartBit { get; }
        uint BitCount { get; }
        double Factor { get; }
        uint Offset { get; }
        string Unit { get; set; }
        double Value { get; set; }
        SignalType SignalType { get; set; }
    }
}