﻿using System.Collections.ObjectModel;
using CANgoUi.Core;
using CANgoUi.Models.DataTypes.Dbf.Impl;
using CANgoUi.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace CANgoUi.ViewModels
{
    public class EditMessageViewModel : ViewModelBase
    {
        // Private
        private readonly DbfService _dbfService;

        // Public 
        public uint Id { get; set; }
        public string Description { get; set; }
        public ObservableCollection<int> ValidMsgDcl { get; }
        public int SelectedDcl { get; set; }

        // Commands
        public RelayCommand<IClosable> UpdateMessageCmd => new RelayCommand<IClosable>(UpdateMessage);

        // Constructor
        public EditMessageViewModel(DbfService dbfService)
        {
            ValidMsgDcl = new ObservableCollection<int> {0, 1, 2, 3, 4, 5, 6, 7, 8};

            _dbfService = dbfService;
        }

        /// <summary>
        /// Update message
        /// </summary>
        public void UpdateMessage(IClosable window)
        {
            var msg = new DbfMessage(Id);
            msg.Dcl = SelectedDcl;
            msg.Description = Description;

            _dbfService.UpdateMessage(msg);
            
            window.Close();
        }
    }
}