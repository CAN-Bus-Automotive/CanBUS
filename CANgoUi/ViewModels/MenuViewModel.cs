﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;
using CANgoUi.Core;
using CANgoUi.Helpers;
using CANgoUi.Models.DataTypes;
using CANgoUi.Services;
using CANgoUi.Views;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Microsoft.Win32;
using NLog;

namespace CANgoUi.ViewModels
{
    public class MenuViewModel : ViewModelBase
    {
        // Common options
        private readonly Logger _log = LogManager.GetCurrentClassLogger();

        // Private
        private readonly SerialPortService _serialPortService;
        private readonly ConnectionService _connectionService;
        private readonly MessageService _messageService;
        private readonly FrameBufferService _bufferService;
        private readonly FileService _fileService;
        private readonly ProgressViewModel _progress;

        // Public
        public ObservableCollection<string> PortList { get; set; } = new ObservableCollection<string>();
        public bool CanChangePort { get; set; } = true;

        // Commands
        public RelayCommand UpdatePortListCmd => new RelayCommand(UpdatePortList);
        public RelayCommand<string> SetPortCmd => new RelayCommand<string>(SetPort);
        public RelayCommand SaveTraceCmd => new RelayCommand(SaveTrace);
        public AsyncCommand OpenTraceCmd => new AsyncCommand(OpenTraceAsync);
        public RelayCommand ShowAboutCmd => new RelayCommand(About);
        public RelayCommand ShowDbfEditorCmd => new RelayCommand(ShowDbfEditor);

        // Constructor
        public MenuViewModel(SerialPortService serialPortService,
            ConnectionService connectionService,
            MessageService messageService,
            FileService fileService,
            FrameBufferService bufferService, ProgressViewModel progress)
        {
            _serialPortService = serialPortService;
            _connectionService = connectionService;
            _messageService = messageService;
            _fileService = fileService;
            _bufferService = bufferService;
            _progress = progress;

            _connectionService.ConnectionStatusChanged += _connectionService_ConnectionStatusChanged;
        }

        private void _connectionService_ConnectionStatusChanged(ConnectionStatus connStatus)
        {
            if (connStatus == ConnectionStatus.ONLINE)
            {
                CanChangePort = false;
            }
            else
            {
                CanChangePort = true;
            }
        }

        private void UpdatePortList()
        {
            PortList.Clear();
            foreach (var port in _serialPortService.GetPortList())
            {
                PortList.Add(port);
            }
        }

        public void SetPort(string portName)
        {
            var cleanPortName = _serialPortService.SetPortName(portName);
            if (!string.IsNullOrWhiteSpace(cleanPortName))
            {
                _connectionService.SelectedPort = cleanPortName;
            }
            else
            {
                _messageService.ShowError("Wrong port name.");
            }
        }

        public void SaveTrace()
        {
            var saveFileDialog = DialogFactory.GetSaveFileDialog(FileService.CanBuffFileFilter);
            saveFileDialog.FileName = FileService.GeDefaultFileBufferName();

            if (saveFileDialog.ShowDialog() == true)
            {
                try
                {
                    _fileService.SaveBufferFile(saveFileDialog.FileName, _bufferService);
                    _log.Info(
                        $"Saved buffer file: '{saveFileDialog.FileName}', with frame count: {_bufferService.CANFrames.Count}");
                }
                catch (Exception e)
                {
                    _log.Error(e);
                    _messageService.ShowError(e.Message);
                }
            }
        }

        public async Task OpenTraceAsync()
        {
            var openFileDialog = DialogFactory.GetFileOpenDialog(FileService.CanBuffFileFilter);

            if (openFileDialog.ShowDialog() == true)
            {
                _bufferService.ClearBuffers();
                _progress.ShowProgress();
                try
                {
                    _bufferService.ClearBuffers();
                    await Task.Run(() =>
                    {
                        var buffer = _fileService.OpenBufferFile(openFileDialog.FileName);
                        _log.Info(
                            $"Open buffer file: '{openFileDialog.FileName}', with frame count: {buffer.CANFrames.Count}");

                        _bufferService.InsertFromBuffer(buffer);
                    });
                    _log.Info($"All frames loaded in view");
                }
                catch (Exception e)
                {
                    _log.Error(e.Message);
                    _messageService.ShowError("Invalid file.");
                }

                _progress.HideProgress();
            }
        }

        /// <summary>
        /// Show about window
        /// </summary>
        public void About()
        {
            AboutView aboutView = new AboutView();
            aboutView.Owner = Application.Current.MainWindow;
            aboutView.WindowStartupLocation = WindowStartupLocation.CenterOwner;

            aboutView.ShowDialog();
        }

        /// <summary>
        /// Show DBF editor
        /// </summary>
        public void ShowDbfEditor()
        {
            DbfEditorView aboutView = new DbfEditorView();
            aboutView.Owner = Application.Current.MainWindow;
            aboutView.WindowStartupLocation = WindowStartupLocation.CenterOwner;

            aboutView.Show();
        }
    }
}