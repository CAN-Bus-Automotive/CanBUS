using System;
using System.Globalization;
using System.Windows.Data;
using CANgoUi.Helpers;

namespace CANgoUi.Converters
{
    public class EnumConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return "";

            foreach (Enum one in Enum.GetValues(parameter as Type))
            {
                if (value.Equals(one))
                    return one.GetDescription();
            }

            return "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return null;

            foreach (Enum one in Enum.GetValues(parameter as Type))
            {            
                if (value.ToString() == one.GetDescription())
                    return one;
            }

            return null;
        }
    }
}