﻿using System.Text;
using System.Threading.Tasks;
using CANFrameLib;
using CANgoUi.Mappers;
using CANgoUi.Models.DataTypes;
using LawicelLib;
using Moq;
using NUnit.Framework;

namespace UiTests
{
    [TestFixture]
    public class MapperTests
    {
        private const string DefaultIncomingFrame = "t1234001122331234\r";
        private const string ExtIncomingFrame = "T123456784001122331234\r";
        private CANFrame _canFrame;
        private ObservableCANFrame _observableCANFrame;
        private TxCANFrame _txCANFrame;
        private Mock<OneChanelLawicel> _lawicelMock;
       

        public MapperTests()
        {
            
        }

        [SetUp]
        public void Setup()
        {
            // Mock 
            _lawicelMock = new Mock<OneChanelLawicel>();
            _lawicelMock.Setup((l) => l.Open("COM1", 115200)).Callback((string p, uint s) => { });
            _lawicelMock.Setup((l) => l.GetVersion()).Returns(() => Task.FromResult<string>("V123"));
            _lawicelMock.Setup((l) => l.SetTimestamp(false)).Callback((bool i) => { });
            _lawicelMock.Setup((l) => l.SetDefaultBaudrate("500")).Callback((string baud) => { });
            
            _canFrame = new CANFrame
            {
                Id = 0x111,
                Dcl = 8,
                Data = new byte[8] {0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77},
                Direction = CanFrameDirections.RX,
                TimeStamp = 0x3333
            };

            _observableCANFrame = new ObservableCANFrame
            {
                Id = 0x5A0,
                Dcl = 5,
                Data = new ObservableItem[5]
                {
                    new ObservableItem {Item = 0xaa},
                    new ObservableItem {Item = 0xbb},
                    new ObservableItem {Item = 0xcc},
                    new ObservableItem {Item = 0xdd},
                    new ObservableItem {Item = 0xee}
                },
                Comment = "Test frame",
                Period = 50,
                TimeStamp = 0x1111
            };

            _txCANFrame = new TxCANFrame(_lawicelMock.Object)
            {
                Frame = _observableCANFrame,
                Is29Bits = false,
                IsRunning = false,
                Period = 50,
                UseIdIncrement = false,
                UseInterval = false
            };
        }


        [Test]
        public void MapToCANFrameTest_returnValidDefaultCANFrame_fromLawicelString()
        {
            var actCANFrame = DefaultIncomingFrame.MapToCANFrame();

            Assert.That(actCANFrame.Id == 0x123);
            Assert.That(actCANFrame.Dcl == 4);
            Assert.That(actCANFrame.Data[0] == 0x00);
            Assert.That(actCANFrame.Data[1] == 0x11);
            Assert.That(actCANFrame.Data[2] == 0x22);
            Assert.That(actCANFrame.Data[3] == 0x33);
            Assert.That(actCANFrame.TimeStamp == 0x1234);
        }

        [Test]
        public void MapToCANFrameTest_returnValidExtCANFrame_fromLawicelString()
        {
            var actCANFrame = ExtIncomingFrame.MapToCANFrame();

            Assert.That(actCANFrame.Id == 0x12345678);
            Assert.That(actCANFrame.Dcl == 4);
            Assert.That(actCANFrame.Data[0] == 0x00);
            Assert.That(actCANFrame.Data[1] == 0x11);
            Assert.That(actCANFrame.Data[2] == 0x22);
            Assert.That(actCANFrame.Data[3] == 0x33);
            Assert.That(actCANFrame.TimeStamp == 0x1234);
        }


        [Test]
        public void MapToObservableCanFrameTest_returnNewValidObservableCANFrame()
        {
            var actObsevableCANFrame = _canFrame.MapToObservableCanFrame();

            Assert.That(actObsevableCANFrame.Id == _canFrame.Id);
            Assert.That(actObsevableCANFrame.Dcl == _canFrame.Dcl);
            Assert.That(actObsevableCANFrame.Data[0].Item == _canFrame.Data[0]);
            Assert.That(actObsevableCANFrame.Data[1].Item == _canFrame.Data[1]);
            Assert.That(actObsevableCANFrame.Data[2].Item == _canFrame.Data[2]);
            Assert.That(actObsevableCANFrame.Data[3].Item == _canFrame.Data[3]);
            Assert.That(actObsevableCANFrame.Data[4].Item == _canFrame.Data[4]);
            Assert.That(actObsevableCANFrame.Data[5].Item == _canFrame.Data[5]);
            Assert.That(actObsevableCANFrame.Data[6].Item == _canFrame.Data[6]);
            Assert.That(actObsevableCANFrame.Data[7].Item == _canFrame.Data[7]);
            Assert.That(actObsevableCANFrame.TimeStamp == _canFrame.TimeStamp);
        }


        [Test]
        public void MapToByteArrayTest_returnValidByteArray_fromTxCANFrame()
        {
            //Arr
            var lwString = "t5A05AABBCCDDEE\r";
            
            var actByteArray = _txCANFrame.MapToByteArray();
            var expByteArray = Encoding.UTF8.GetBytes(lwString);
            
            Assert.That(actByteArray, Is.EqualTo(expByteArray));
        }
    }
}