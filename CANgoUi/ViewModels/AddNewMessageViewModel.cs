﻿using CANgoUi.Core;
using CANgoUi.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System.Collections.ObjectModel;
using System.Linq;
using CANgoUi.Models.DataTypes.Dbf.Impl;

namespace CANgoUi.ViewModels
{
    public class AddNewMessageViewModel : ViewModelBase
    {
        // Private
        private readonly DbfService _dbfService;
        private readonly MessageService _messageService;

        // Public
        public ObservableCollection<int> ValidMsgDcl { get; }
        public uint Id { get; set; } = 0x0;
        public string Description { get; set; }
        public int SelectedDcl { get; set; } = 0;

        // Commands
        public RelayCommand<IClosable> AddNewMessageCmd => new RelayCommand<IClosable>(AddNewMessage);

        // Constructor
        public AddNewMessageViewModel(DbfService dbfService, MessageService messageService)
        {
            ValidMsgDcl = new ObservableCollection<int> {0, 1, 2, 3, 4, 5, 6, 7, 8};

            _dbfService = dbfService;
            _messageService = messageService;
        }

        /// <summary>
        /// Add new message message collection
        /// </summary>
        /// <param name="closableWindow"></param>
        public void AddNewMessage(IClosable closableWindow)
        {
            var msg = new DbfMessage(Id);
            msg.Description = Description;
            msg.Dcl = SelectedDcl;

            var msgm = _dbfService.DbfMessages.FirstOrDefault(m => m.Id == msg.Id);

            if (msgm != null)
            {
                _messageService.ShowError("Duplicate IDs");
            }
            else
            {
                _dbfService.DbfMessages.Add(msg);

                // Close owned window
                closableWindow.Close();
            }
        }


        public void EditSelectedMessage(IClosable closableWindow)
        {
            
        }
    }
}