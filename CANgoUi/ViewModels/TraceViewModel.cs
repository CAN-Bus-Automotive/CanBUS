﻿using System;
using System.IO;
using System.Threading.Tasks;
using CANFrameLib;
using CANgoUi.Helpers;
using CANgoUi.Mappers;
using CANgoUi.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Threading;
using NLog;

namespace CANgoUi.ViewModels
{
    public class TraceViewModel : ViewModelBase
    {
        // Public
        public string RecPauseText { get; set; } = "Rec";
        public bool CanReplay { get; set; } = true;
        public CANFrame SelectedItem { get; set; }
        public string CurrentChunkFileName { get; set; } = string.Empty;

        // Private
        private readonly Logger _log = LogManager.GetCurrentClassLogger();
        private readonly TraceService _traceService;
        private readonly TransmitService _transmitService;
        private readonly MessageService _messageService;
        private readonly FileService _fileService;

        // Commands
        public RelayCommand RecPauseCmd => new RelayCommand(RecPause);
        public RelayCommand ReplayCmd => new RelayCommand(Replay);
        public RelayCommand StopReplayCmd => new RelayCommand(StopReplay);
        public RelayCommand StepOneCmd => new RelayCommand(StepOne);
        public RelayCommand ResetCmd => new RelayCommand(_traceService.Reset);
        public RelayCommand SaveChunkCmd => new RelayCommand(SaveChank);
        public RelayCommand OpenChunkCmd => new RelayCommand(OpenChunkAsync);
        public RelayCommand CopyToTxListCmd => new RelayCommand(CopyToTxList);

        // Constructor
        public TraceViewModel(TraceService traceService, TransmitService transmitService,
            MessageService messageService, FileService fileService)
        {
            _traceService = traceService;
            _transmitService = transmitService;
            _messageService = messageService;
            _fileService = fileService;
        }

        /// <summary>
        /// Record(pause) frame chunk
        /// </summary>
        [CheckConnection]
        public void RecPause()
        {
            if (_traceService.IsRec)
            {
                _traceService.IsRec = false;
                CanReplay = true;
                RecPauseText = "Rec";
            }
            else
            {
                // TODO Refactor this...
                _traceService.ClearTraceBuffer();
                CanReplay = false;
                _traceService.IsRec = true;
                RecPauseText = "Pause";
                CurrentChunkFileName = string.Empty;
                _traceService.CurrentItem = 0;
            }
        }

        /// <summary>
        /// Start replay
        /// </summary>
        [CheckConnection]
        public void Replay()
        {
            _transmitService.StopAllFrames();
            _traceService.RunReplay();
        }

        /// <summary>
        /// Stop replaing
        /// </summary>
        public void StopReplay()
        {
            _traceService.StopReplay();
        }

        /// <summary>
        /// Make One stepy through tracevuffer
        /// </summary>
        [CheckConnection]
        public void StepOne()
        {
            _transmitService.StopAllFrames();
            _traceService.StepOne();
        }

        /// <summary>
        /// Save trace chunk
        /// </summary>
        public void SaveChank()
        {
            var saveFileDialog = DialogFactory.GetSaveFileDialog(FileService.ChunkFileFilter);
            saveFileDialog.FileName = FileService.GetDefaultChunkName();

            if (saveFileDialog.ShowDialog() == true)
            {
                try
                {
                    _fileService.SaveChunkFile(saveFileDialog.FileName, _traceService.TraceBuffer);
                    _log.Info(
                        $"Saved chunk file: '{saveFileDialog.FileName}', with frame count: {_traceService.TraceBuffer.Count}");
                }
                catch (Exception e)
                {
                    _log.Error(e);
                    _messageService.ShowError(e.Message);
                }
            }
        }

        /// <summary>
        /// Open trace chunk
        /// </summary>
        public async void OpenChunkAsync()
        {
            var openFileDialog = DialogFactory.GetFileOpenDialog(FileService.ChunkFileFilter);

            if (openFileDialog.ShowDialog() == true)
            {
                _traceService.ClearTraceBuffer();
                try
                {
                    _traceService.ClearTraceBuffer();
                    await Task.Run(() =>
                    {
                        var buffer = _fileService.OpenChunkFile(openFileDialog.FileName);
                        _log.Info(
                            $"Open chunk file: '{openFileDialog.FileName}', with frame count: {_traceService.TraceBuffer.Count}");

                        DispatcherHelper.RunAsync(() => { _traceService.TraceBuffer.AddRange(buffer); });

                        CurrentChunkFileName = Path.GetFileName(openFileDialog.FileName);
                        _traceService.CurrentItem = 0;
                    });
                    _log.Info($"All frames from chunk loaded in view");
                }
                catch (Exception e)
                {
                    _log.Error(e.Message);
                    _messageService.ShowError("Invalid file.");
                }
            }
        }

        /// <summary>
        /// Copy selected item to TX list
        /// </summary>
        public void CopyToTxList()
        {
            if (SelectedItem != null)
            {
                _transmitService.TxFrames.Add(SelectedItem.MapToTxCANFrame());
            }
        }
    }
}