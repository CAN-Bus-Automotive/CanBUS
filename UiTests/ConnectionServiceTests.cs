﻿using System.Threading.Tasks;
using CANgoUi.Models.DataTypes;
using CANgoUi.Services;
using LawicelLib;
using Moq;
using NUnit.Framework;

namespace UiTests
{
    [TestFixture]
    public class ConnectionServiceTests
    {
        private Mock<OneChanelLawicel> _lawicelMock;
        private Mock<MessageService> _messageServiceMock;
        private ConnectionService _connctionService;


        [SetUp]
        public void SetUp()
        {
            // Mock 
            _lawicelMock = new Mock<OneChanelLawicel>();
            _lawicelMock.Setup((l) => l.Open("COM1", 115200)).Callback((string p, uint s) => { });
            _lawicelMock.Setup((l) => l.GetVersion()).Returns(() => Task.FromResult<string>("V123"));
            _lawicelMock.Setup((l) => l.SetTimestamp(false)).Callback((bool i) => { });
            _lawicelMock.Setup((l) => l.SetDefaultBaudrate("500")).Callback((string baud) => { });
            //_lawicelMock.Setup((l) => l.CloseChanel()).Returns(() => new Task(() => { Task.Delay(1); }));

            _messageServiceMock = new Mock<MessageService>();
        }

        [Test]
        public void ConnectAsynctTest_ValidConnectionState()
        {
            // Arr
            _connctionService = new ConnectionService(_lawicelMock.Object, _messageServiceMock.Object);

            // Act
            _connctionService.ConnectAsync().Wait();

            Assert.That(_connctionService.ConnectionStatus, Is.EqualTo(ConnectionStatus.ONLINE));
        }

        [Test]
        public void DisconnectTest_ValidConnectionState()
        {
            // Arr
            _connctionService = new ConnectionService(_lawicelMock.Object, _messageServiceMock.Object);
            _connctionService.ConnectionStatus = ConnectionStatus.ONLINE;
            
            // Act
            _connctionService.Disconnect();
            
            // Assert
            Assert.That(_connctionService.ConnectionStatus, Is.EqualTo(ConnectionStatus.OFFLINE));
        }
    }
}