﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using CANgoUi.Helpers;
using CANgoUi.Models.DataTypes.Dbf.Impl;
using GalaSoft.MvvmLight;
using Microsoft.Win32;
using NLog;

namespace CANgoUi.Services
{
    public class DbfService : ViewModelBase
    {
        // Utils
        private readonly Logger _log = NLog.LogManager.GetCurrentClassLogger();

        // Private
        private readonly MessageService _messageService;
        private readonly FileService _fileService;

        public ObservableCollection<DbfMessage> DbfMessages { get; set; }
        public ObservableCollection<DbfMessage> ActiveDbfMessages { get; set; }
        public string OpenedDbfFileName { get; private set; }
        public string ActiveDbfFileName { get; private set; }

        public DbfService(MessageService messageService, FileService fileService)
        {
            _messageService = messageService;
            _fileService = fileService;

            DbfMessages = new ObservableCollection<DbfMessage>();
            ActiveDbfMessages = new ObservableCollection<DbfMessage>();
        }


        public void DeleteMessage(DbfMessage message)
        {
            DbfMessages.Remove(message);
        }

        public void UpdateMessage(DbfMessage message)
        {
            var first = DbfMessages.FirstOrDefault(m => m.Id == message.Id);

            first.Dcl = message.Dcl;
            first.Description = message.Description;
        }


        public void SaveDatabase()
        {
            var saveFileDialog = DialogFactory.GetSaveFileDialog(FileService.DbfFileFilter);
            saveFileDialog.FileName = FileService.GetDefaultDatabaseName();
            
            if (saveFileDialog.ShowDialog() == true)
            {
                try
                {
                    _fileService.SaveDbfFile(saveFileDialog.FileName, DbfMessages);

                    OpenedDbfFileName = saveFileDialog.FileName;
                    _log.Info(
                        $"Saved TX lis file: '{saveFileDialog.FileName}', with frame count: {DbfMessages.Count}");
                }
                catch (Exception e)
                {
                    _log.Error(e);
                    _messageService.ShowError(e.Message);
                }
            }
        }

        public void OpenDatabase()
        {
            var openFileDialog = DialogFactory.GetFileOpenDialog(FileService.DbfFileFilter);
            
            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    var buffer = _fileService.OpenDbfFile(openFileDialog.FileName);
                    _log.Info(
                        $"Open database file: '{openFileDialog.FileName}");

                    DbfMessages.Clear();
                    DbfMessages = buffer;

                    OpenedDbfFileName = openFileDialog.FileName;

                    _log.Info($"All frames loaded in view");
                }
                catch (Exception e)
                {
                    _log.Error(e.Message);
                    _messageService.ShowError("Invalid file.");
                }
            }
        }

        public void AssociateDatabase()
        {
            var openFileDialog = DialogFactory.GetFileOpenDialog(FileService.DbfFileFilter);

            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    var buffer = _fileService.OpenDbfFile(openFileDialog.FileName);
                    _log.Info(
                        $"Open database file: '{openFileDialog.FileName}");

                    ActiveDbfMessages.Clear();
                    ActiveDbfMessages = buffer;

                    ActiveDbfFileName = openFileDialog.FileName;

                    _log.Info($"DBF file loaded successfully");
                }
                catch (Exception e)
                {
                    _log.Error(e.Message);
                    _messageService.ShowError("Invalid file");
                }
            }
        }

        public void SaveAndApplyDatabase()
        {
            // Save changes to current opened file
            if (string.IsNullOrEmpty(OpenedDbfFileName))
            {
                SaveDatabase();
            }
            else
            {
                try
                {
                    _fileService.SaveDbfFile(OpenedDbfFileName, DbfMessages);
                    _log.Info(
                        $"Saved TX lis file: '{OpenedDbfFileName}', with frame count: {DbfMessages.Count}");
                }
                catch (Exception e)
                {
                    _log.Error(e);
                    _messageService.ShowError(e.Message);
                }
            }

            // apply changes to DBF View
            try
            {
                _log.Info(
                    $"Open database file: '{OpenedDbfFileName}");
                var buffer = _fileService.OpenDbfFile(OpenedDbfFileName);

                ActiveDbfMessages.Clear();
                ActiveDbfMessages = buffer;

                ActiveDbfFileName = OpenedDbfFileName;

                _log.Info($"DBF file loaded successfully");
            }
            catch (Exception e)
            {
                _log.Error(e.Message);
                _messageService.ShowError("Invalid file");
            }
        }
    }
}