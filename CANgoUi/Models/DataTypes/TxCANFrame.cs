﻿using System.ComponentModel;
using System.Threading;
using CANgoUi.Mappers;
using CommonServiceLocator;
using GalaSoft.MvvmLight;
using LawicelLib;

namespace CANgoUi.Models.DataTypes
{
    public class TxCANFrame : ViewModelBase
    {
        // Private
        private BackgroundWorker _worker;
        private readonly Lawicel _lw;
        
        // Public
        public const uint Id29BitMaxValue = 0x1FFFFFFF;
        public const uint Id11BitMaxValue = 0x7FF;
        public ObservableCANFrame Frame { get; set; }
        public uint Period { get; set; }
        public bool Is29Bits { get; set; }
        public bool UseIdIncrement { get; set; }
        public bool UseInterval { get; set; }
        public bool IsRunning { get; set; }
        public int TransmissionCount { get; set; }

        public TxCANFrame()
        {
            if (_lw == null)
            {
                _lw =  ServiceLocator.Current.GetInstance<Lawicel>();
            }
        }

        public TxCANFrame(Lawicel lw)
        {
            _lw = lw;
        }
        
        public void SendByPeriod()
        {
            _worker = new BackgroundWorker();
            _worker.WorkerSupportsCancellation = true;
            _worker.DoWork += Worker_DoWorkAsync;
            _worker.RunWorkerAsync();
        }

        public void StopSending()
        {
            _worker.CancelAsync();
            TransmissionCount = 0;
        }

        private async void Worker_DoWorkAsync(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker bw = sender as BackgroundWorker;

            while (!bw.CancellationPending)
            {
                if (UseIdIncrement)
                {
                    if (Is29Bits)
                    {
                        if (Frame.Id <= Id29BitMaxValue)
                        {
                            Frame.Id++;
                        }
                        else
                        {
                            Frame.Id = 0;
                        }
                    }
                    else
                    {
                        if (Frame.Id <= Id11BitMaxValue)
                        {
                            Frame.Id++;
                        }
                        else
                        {
                            Frame.Id = 0;
                        }
                    }
                }

                for (int i = 0; i < Frame.Dcl; i++)
                {
                    if (Frame.Data[i].UseIncrement)
                    {
                        Frame.Data[i].Item++;
                    }
                }

                TransmissionCount++;

                await _lw.SendTxFrame(this.MapToByteArray());

                // TODO add frame send this
                Thread.Sleep((int) Period);
            }
        }
    }
}