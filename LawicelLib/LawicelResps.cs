﻿namespace LawicelLib
{
    public static class LawicelResps
    {
        /// <summary>
        /// OK
        /// </summary>
        public const byte CR = 0x0D;

        /// <summary>
        /// Error
        /// </summary>
        public const byte BELL = 0x07;
    }
}
