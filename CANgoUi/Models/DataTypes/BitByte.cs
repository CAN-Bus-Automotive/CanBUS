﻿using GalaSoft.MvvmLight;

namespace CANgoUi.Models.DataTypes
{
    public class BitByte : ViewModelBase
    {
        // Public 
        public decimal Bit0 { get; set; }
        public decimal Bit1 { get; set; }
        public decimal Bit2 { get; set; }
        public decimal Bit3 { get; set; }
        public decimal Bit4 { get; set; }
        public decimal Bit5 { get; set; }
        public decimal Bit6 { get; set; }
        public decimal Bit7 { get; set; }


        /// <summary>
        /// Represent byte as bits
        /// </summary>
        /// <param name="value"></param>
        public void SetBitByte(byte value)
        {
            Bit0 = (value & (1 << 0)) >> 0;
            Bit1 = (value & (1 << 1)) >> 1;
            Bit2 = (value & (1 << 2)) >> 2;
            Bit3 = (value & (1 << 3)) >> 3;
            Bit4 = (value & (1 << 4)) >> 4;
            Bit5 = (value & (1 << 5)) >> 5;
            Bit6 = (value & (1 << 6)) >> 6;
            Bit7 = (value & (1 << 7)) >> 7;
        }
    }
}