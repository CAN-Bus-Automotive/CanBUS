﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Xml.Serialization;
using CANFrameLib;
using CANgoUi.Models.DataTypes;
using CANgoUi.Models.DataTypes.Dbf.Impl;

namespace CANgoUi.Services
{
    public class FileService
    {
        public const string DefaultFilePath = "buffer_1.canbuf";

        public static string GeDefaultFileBufferName()
        {
            return $"general_log_{DateTime.Now:_MMddyyyy_HHmmss}.canbuf";
        }

        public static string GeDefaultFileTxListName()
        {
            return $"tx_list_{DateTime.Now:_MMddyyyy_HHmmss}.txlist";
        }

        public static string GetDefaultDatabaseName()
        {
            return $"database_{DateTime.Now:_MMddyyyy_HHmmss}.dbfx";
        }
        
        public static string GetDefaultChunkName()
        {
            return $"chunk_{DateTime.Now:_MMddyyyy_HHmmss}.chunk";
        }

        public const string ChunkFileFilter = "Text file (*.chunk)|*.chunk";
        public const string CanBuffFileFilter = "Text file (*.canbuf)|*.canbuf";
        public const string TxListFileFilter = "Text file (*.txlist)|*.txlist";
        public const string DbfFileFilter = "Database file (*.dbfx)|*.dbfx";
        
        /// <summary>
        /// Open frame buffer file
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public FrameBufferService OpenBufferFile(string filePath)
        {
            var result = new FrameBufferService();
            using (StreamReader file = File.OpenText(filePath))
            {
                var serializer = new XmlSerializer(typeof(FrameBufferService));
                result = (FrameBufferService) serializer.Deserialize(file);
            }

            return result;
        }

        /// <summary>
        /// Open TX list file
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public List<TxCANFrame> OpenTxListFile(string filePath)
        {
            var result = new List<TxCANFrame>();
            using (StreamReader file = File.OpenText(filePath))
            {
                var serializer = new XmlSerializer(typeof(List<TxCANFrame>));
                result = (List<TxCANFrame>) serializer.Deserialize(file);
            }

            return result;
        }

        /// <summary>
        /// Open frame buffer file
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="dataSource"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public List<CANFrame> OpenFile(string filePath, object dataSource)
        {
            throw new System.NotImplementedException();
        }

        public void SaveBufferFile(object dataSource)
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Save frame buffer file
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="dataSource"></param>
        public void SaveBufferFile(string filePath, object dataSource)
        {
            using (StreamWriter file = File.CreateText(filePath))
            {
                var serializer = new XmlSerializer(typeof(FrameBufferService));
                serializer.Serialize(file, dataSource);
            }
        }

        /// <summary>
        /// Save TX list file
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="dataSource"></param>
        public void SaveTxListFile(string filePath, object dataSource)
        {
            using (StreamWriter file = File.CreateText(filePath))
            {
                var serializer = new XmlSerializer(typeof(List<TxCANFrame>));
                serializer.Serialize(file, dataSource);
            }
        }

        /// <summary>
        /// Save trace chunk to file
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="dataSource"></param>
        public void SaveChunkFile(string filePath, object dataSource)
        {
            using (StreamWriter file = File.CreateText(filePath))
            {
                var serializer = new XmlSerializer(typeof(ObservableRangeCollection<CANFrame>));
                serializer.Serialize(file, dataSource);
            }
        }
        
        /// <summary>
        /// Open chunk file
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns>ObservableCollection of CANFrame</returns>
        public ObservableCollection<CANFrame> OpenChunkFile(string filePath)
        {
            var result = new ObservableRangeCollection<CANFrame>();
            using (StreamReader file = File.OpenText(filePath))
            {
                var serializer = new XmlSerializer(typeof(ObservableRangeCollection<CANFrame>));
                result = (ObservableRangeCollection<CANFrame>) serializer.Deserialize(file);
            }

            return result;
        }

        /// <summary>
        /// Save database file
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="dataSource"></param>
        public void SaveDbfFile(string filePath, object dataSource)
        {
            using (StreamWriter file = File.CreateText(filePath))
            {
                var serializer = new XmlSerializer(typeof(ObservableCollection<DbfMessage>));
                serializer.Serialize(file, dataSource);
            }
        }

        /// <summary>
        /// Open database file
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns>ObservableCollection of DbfMessage</returns>
        public ObservableCollection<DbfMessage> OpenDbfFile(string filePath)
        {
            var result = new ObservableCollection<DbfMessage>();
            using (StreamReader file = File.OpenText(filePath))
            {
                var serializer = new XmlSerializer(typeof(ObservableCollection<DbfMessage>));
                result = (ObservableCollection<DbfMessage>) serializer.Deserialize(file);
            }

            return result;
        }
    }
}