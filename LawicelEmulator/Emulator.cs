﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Threading;

namespace LawicelEmulator
{
    public class Emulator
    {
        private SerialPort _sp;
        private string incomingData;
        private EmulatorConfig _cfg;
        private List<EmulatedFrame> Frames = new List<EmulatedFrame>();
        private bool _canStart = false;

        public Emulator(EmulatorConfig cfg)
        {
            _cfg = cfg;
            Init(_cfg);
        }

        public void Emulate()
        {
            _sp.Open();

            Random rand = new Random();

            while (true)
            {
                foreach (var f in Frames)
                {
                   
                    for (int i = 0; i < rand.Next(f.Dcl); i++)
                    {
                        f.Data[rand.Next(f.Dcl)] = (byte)rand.Next(0xFF);
                    }
                    _sp.Write(f.ToString());

                    Thread.Sleep((int)f.TimeStamp);
                    Console.WriteLine(f);
                }
            }
           
        }

        private void Init(EmulatorConfig cfg)
        {
            _sp = new SerialPort
            {
                PortName = cfg.PortName,
                BaudRate = 115200
            };

            _sp.DataReceived += _sp_DataReceived;

            GenerateFrames(cfg);
        }

        private void GenerateFrames(EmulatorConfig cfg)
        {
            Random rand = new Random();

            for (int i = 0; i < cfg.UniqFramesCount; i++)
            {
                EmulatedFrame frame = new EmulatedFrame();
                frame.Id = (byte)rand.Next(0x7FF);
                frame.Dcl = rand.Next(1, 8);
                frame.Data = new byte[frame.Dcl];
                for (int j = 0; j < frame.Dcl; j++)
                {
                    frame.Data[j] = (byte)rand.Next(0xFF);
                }
                frame.TimeStamp = (uint)rand.Next(800);

                Frames.Add(frame);
            }
        }

        private void _sp_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            incomingData = _sp.ReadExisting();
        }
    }
}
