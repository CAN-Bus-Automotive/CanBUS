﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace CANgoUi.Converters
{
    public class BoolToOnOffConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((bool) value == true)
            {
                return "ON";
            }
            else
            {
                return "OFF";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}