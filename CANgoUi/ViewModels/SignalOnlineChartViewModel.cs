﻿using System;
using GalaSoft.MvvmLight;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Threading;
using CANgoUi.Models.DataTypes.Dbf.Impl;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;

namespace CANgoUi.ViewModels
{
    public class SignalOnlineChartViewModel : ViewModelBase
    {
        // Private
        private List<OxyColor> SetColors { get; set; }
        private List<double> _onlineValueList = new List<double>();
        private LinearAxis _yAxis;
        private LinearAxis _xAxis;
        private LineSeries _lineSeries;
        private readonly DispatcherTimer _timer;
        private int _timeLine = 0;
        private int _lastIndex = 0;
        private double _lastChartValue = 0;
        private const int TimerInterval = 200;
        private const int TimelineMaximum = 20000;

        // Public
        public PlotModel ChartModel { get; set; }
        public string Title { get; set; }
        public Signal SelectedSignal { get; set; }

        // Commands

        // Constructor
        public SignalOnlineChartViewModel(Signal signal)
        {
            ChartModel = new PlotModel();
            SelectedSignal = signal;

            SetColors = new List<OxyColor>
            {
                OxyColors.Red,
                OxyColors.Orange,
                OxyColors.Yellow,
                OxyColors.Green,
                OxyColors.Blue,
                OxyColors.Indigo,
                OxyColors.Violet,
                OxyColors.Azure
            };

            ViewInit();

            Title = $"Chart for Signal: {SelectedSignal.Name}";

            _timer = new DispatcherTimer();
            _timer.Interval = TimeSpan.FromMilliseconds(TimerInterval);
            _timer.Tick += TimerOnElapsed;
            _timer.Start();
        }

        private async void TimerOnElapsed(object sender, EventArgs e)
        {
            _onlineValueList = SelectedSignal.ValueLog.GetRange(_lastIndex, SelectedSignal.ValueLog.Count - _lastIndex);
            _lastIndex = SelectedSignal.ValueLog.Count;
            await UpdateChart();
            ChartModel.InvalidatePlot(true);
        }

        private void ViewInit()
        {
            ChartModel.LegendTitle = "Legend";
            ChartModel.LegendOrientation = LegendOrientation.Vertical;
            ChartModel.LegendPlacement = LegendPlacement.Inside;
            ChartModel.LegendPosition = LegendPosition.TopRight;
            ChartModel.LegendBackground = OxyColor.FromAColor(200, OxyColors.White);
            ChartModel.LegendBorder = OxyColors.Black;
            ChartModel.PlotAreaBorderColor = OxyColors.LightGray;

            _yAxis = new LinearAxis()
            {
                ExtraGridlineColor = OxyColors.LightGray,
                TextColor = OxyColors.LightGray,
                MajorGridlineColor = OxyColors.DarkSlateGray,
                MinorGridlineColor = OxyColors.DarkSlateGray,
                MajorGridlineStyle = LineStyle.Dot,
                MajorGridlineThickness = 1,
                Title = "Value",
                TitleColor = OxyColors.LightGray,
                Position = AxisPosition.Left,
                LabelFormatter = (d) => $"{d}"
            };
            _xAxis = new LinearAxis()
            {
                Title = "Time",
                TitleColor = OxyColors.LightGray,
                ExtraGridlineColor = OxyColors.LightGray,
                TextColor = OxyColors.LightGray,
                MajorGridlineColor = OxyColors.DarkSlateGray,
                MinorGridlineColor = OxyColors.DarkSlateGray,
                MajorGridlineStyle = LineStyle.Dot,
                MajorGridlineThickness = 1,
                Position = AxisPosition.Bottom,
                Maximum = TimelineMaximum
            };
            ChartModel.Axes.Add(_yAxis);
            ChartModel.Axes.Add(_xAxis);

            Random randomForColor = new Random();

            _lineSeries = new LineSeries()
            {
                Title = $"{SelectedSignal.Name}", StrokeThickness = 3,
                Color = SetColors[randomForColor.Next(0, SetColors.Count)]
            };

            ChartModel.Series.Add(_lineSeries);
        }

        private async Task UpdateChart()
        {
            await Task.Run(() =>
            {
                for (int i = 0; i < TimerInterval; i++)
                {
                    if (i < _onlineValueList.Count && _onlineValueList.Count != 0)
                    {
                        _lineSeries.Points.Add(new DataPoint(_timeLine, _onlineValueList[i]));
                        _lastChartValue = _onlineValueList[i];
                    }
                    else
                    {
                        _lineSeries.Points.Add(new DataPoint(_timeLine, Convert.ToDouble(_lastChartValue)));
                    }

                    if (_lineSeries.Points.Count > TimelineMaximum)
                    {
                        _lineSeries.Points.Clear();
                        _timeLine = 0;
                    }

                    _timeLine++;
                }
            });
        }
    }
}