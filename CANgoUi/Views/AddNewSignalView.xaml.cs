﻿using System.Windows;
using CANgoUi.Core;

namespace CANgoUi.Views
{
    /// <summary>
    /// Interaction logic for AddNewSignalView.xaml
    /// </summary>
    public partial class NewSignalView : Window, IClosable
    {
        public NewSignalView()
        {
            InitializeComponent();
        }
    }
}