﻿using System.Collections.ObjectModel;
using CANFrameLib;
using CANgoUi.Models.DataTypes;
using CANgoUi.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using LawicelLib;
using NLog;

namespace CANgoUi.ViewModels
{
    public class ConnectionViewModel : ViewModelBase
    {
        // Common options
        private readonly Logger _log = LogManager.GetCurrentClassLogger();

        // Private
        private readonly MessageService _messageService;
        private readonly ConnectionService _connectionService;
        private readonly FrameBufferService _bufferService;
        private readonly TransmitService _transmitService;
        private readonly DbfService _dbfService;
        private readonly Lawicel _lawicel;

        // Public
        public ObservableCollection<string> BaudList { get; private set; }
        public bool CanChangeBaudrate { get; set; } = true;

        public string SelectedBaud
        {
            get { return _connectionService.SelectedBaud; }
            set { _connectionService.SelectedBaud = value; }
        }

        public bool CanChangePort { get; set; } = true;
        public string ConnectButtonText { get; set; } = "Start";

        // Commands
        public RelayCommand ConnectCmd => new RelayCommand(ConnectDisconnect);

        // Constructor
        public ConnectionViewModel(MessageService messageService,
            ConnectionService connectionService,
            Lawicel lawicel,
            FrameBufferService bufferService, TransmitService transmitService, DbfService dbfService)
        {
            _messageService = messageService;
            _connectionService = connectionService;
            _bufferService = bufferService;
            _transmitService = transmitService;
            _dbfService = dbfService;
            _lawicel = lawicel;

            BaudList = new ObservableCollection<string>(_connectionService.ValidBaudrates);
            _connectionService.FrameReceived += _connectionService_FrameReceived;
            _connectionService.ConnectionStatusChanged += _connectionService_ConnectionStatusChanged;
        }

        private void _connectionService_ConnectionStatusChanged(ConnectionStatus connectionStatus)
        {
            if (connectionStatus == ConnectionStatus.OFFLINE)
            {
                ConnectButtonText = "Start";
                CanChangeBaudrate = true;
            }
            else
            {
                ConnectButtonText = "Stop";
                CanChangeBaudrate = false;
            }
        }

        private void _connectionService_FrameReceived(CANFrame frame)
        {
            _bufferService.AddToQueue(frame);

            foreach (var message in _dbfService.ActiveDbfMessages)
            {
                if (message.Id == frame.Id && message.Dcl == frame.Dcl)
                {
                    message.UpdateSignals(frame);
                }
            }
        }

        public async void ConnectDisconnect()
        {
            if (_connectionService.ConnectionStatus == ConnectionStatus.OFFLINE)
            {
                await _connectionService.ConnectAsync();
            }
            else
            {
                _transmitService.StopAllFrames();
                _connectionService.Disconnect();
            }
        }
    }
}