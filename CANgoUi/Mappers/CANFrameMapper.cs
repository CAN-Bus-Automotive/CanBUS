﻿using System;
using System.Text;
using CANFrameLib;
using CANgoUi.Helpers;
using CANgoUi.Models.DataTypes;
using LawicelLib;

namespace CANgoUi.Mappers
{
    public static class CANFrameMapper
    {
        /// <summary>
        /// Map lawicel string to CANFrame entity
        /// </summary>
        /// <param name="lawicelString"></param>
        /// <returns></returns>
        public static CANFrame MapToCANFrame(this string lawicelString)
        {
            /* some magic next - see lawicel documentation http://www.can232.com/docs/canusb_manual.pdf */
            CANFrame frame = new CANFrame();
            string cmd = string.Empty;

            /* get cmd from lawicel string */
            cmd = lawicelString.Substring(0, 1);

            switch (cmd)
            {
                case LawicelCmds.t:
                    frame.Id = Convert.ToUInt32(lawicelString.Substring(1, 3), 16); // TODO Delete magic numbers
                    frame.Dcl = Convert.ToInt32(lawicelString.Substring(4, 1));
                    frame.Data = new byte[frame.Dcl];
                    frame.Direction = CanFrameDirections.RX;
                    for (sbyte i = 0, j = 0; i < frame.Dcl; i++, j += 2)
                    {
                        frame.Data[i] = Convert.ToByte(lawicelString.Substring(5 + j, 2), 16);
                    }

                    if (lawicelString.Substring(5 + frame.Dcl * 2, 1) != "\r")
                    {
                        frame.TimeStamp = Convert.ToUInt32(lawicelString.Substring(5 + frame.Dcl * 2, 4), 16);
                    }

                    break;

                case LawicelCmds.T:
                    frame.Id = Convert.ToUInt32(lawicelString.Substring(1, 8), 16); // TODO Delete magic numbers
                    frame.Dcl = Convert.ToInt32(lawicelString.Substring(9, 1));
                    frame.Data = new byte[frame.Dcl];
                    frame.Direction = CanFrameDirections.RX;
                    for (sbyte i = 0, j = 0; i < frame.Dcl; i++, j += 2)
                    {
                        frame.Data[i] = Convert.ToByte(lawicelString.Substring(10 + j, 2), 16);
                    }

                    if (lawicelString.Substring(10 + frame.Dcl * 2, 1) != "\r")
                    {
                        frame.TimeStamp = Convert.ToUInt32(lawicelString.Substring(10 + frame.Dcl * 2, 4), 16);
                    }

                    break;
            }

            return frame;
        }

        /// <summary>
        /// Map CANFrame to ObservableCANFrame
        /// </summary>
        /// <param name="frame"></param>
        /// <returns></returns>
        public static ObservableCANFrame MapToObservableCanFrame(this CANFrame frame)
        {
            ObservableCANFrame obsFrame = new ObservableCANFrame();
            obsFrame.Id = frame.Id;

            ObservableItem[] items = new ObservableItem[frame.Data.Length];
            for (int i = 0; i < frame.Data.Length; i++)
            {
                var dataItem = new ObservableItem {Item = frame.Data[i]};
                items[i] = dataItem;
            }

            obsFrame.Data = items;
            obsFrame.Id = frame.Id;
            obsFrame.Dcl = frame.Dcl;
            obsFrame.Is29Bits = frame.Is29Bit;
            obsFrame.TimeStamp = frame.TimeStamp;

            return obsFrame;
        }

        /// <summary>
        /// Map ObservableCANFrame to TxCANFrame
        /// </summary>
        /// <param name="observableCANFrame"></param>
        /// <returns></returns>
        public static TxCANFrame MapToTxCANFrame(this ObservableCANFrame observableCANFrame)
        {
            var frameFactory = new CANFrameFactory();

            var txFrame = new TxCANFrame
            {
                Frame = new ObservableCANFrame
                {
                    Id = observableCANFrame.Id,
                    Count = observableCANFrame.Count,
                    Dcl = observableCANFrame.Dcl,
                    Comment = observableCANFrame.Comment,
                    Period = observableCANFrame.Period,
                    TimeStamp = observableCANFrame.TimeStamp,
                },
                IsRunning = false,
                Period = observableCANFrame.Period,
                UseIdIncrement = false,
                UseInterval = false
            };

            var items = frameFactory.GetObservableItems();
            for (int i = 0; i < observableCANFrame.Data.Length; i++)
            {
                items[i] = new ObservableItem {Item = observableCANFrame.Data[i].Item};
            }

            txFrame.Frame.Data = items;

            if (observableCANFrame.Id > TxCANFrame.Id11BitMaxValue)
            {
                txFrame.Is29Bits = true;
            }

            return txFrame;
        }

        /// <summary>
        /// Map CANFrame to TxCANFrame
        /// </summary>
        /// <param name="canFrame"></param>
        /// <returns></returns>
        public static TxCANFrame MapToTxCANFrame(this CANFrame canFrame)
        {
            var frameFactory = new CANFrameFactory();
            
            var txFrame = new TxCANFrame
            {
                Frame = new ObservableCANFrame
                {
                    Id = canFrame.Id,
                    Count = 0,
                    Dcl = canFrame.Dcl,
                    Comment = "",
                    Period = 100,
                    TimeStamp = canFrame.TimeStamp,
                },
                IsRunning = false,
                Period = 100,
                UseIdIncrement = false,
                UseInterval = false
            };
            
            var items = frameFactory.GetObservableItems();
            for (int i = 0; i < canFrame.Data.Length; i++)
            {
                items[i] = new ObservableItem {Item = canFrame.Data[i]};
            }
            
            txFrame.Frame.Data = items;
            
            if (canFrame.Id > TxCANFrame.Id11BitMaxValue)
            {
                txFrame.Is29Bits = true;
            }

            return txFrame;
        }

        /// <summary>
        /// Map TxCANFrame To ByteArray
        /// </summary>
        /// <param name="txCANFrame"></param>
        /// <returns></returns>
        public static byte[] MapToByteArray(this TxCANFrame txCANFrame)
        {
            string result = string.Empty;

            if (txCANFrame.Is29Bits)
            {
                result += LawicelCmds.T;
                result += txCANFrame.Frame.Id.ToString("X8");
            }
            else
            {
                result += LawicelCmds.t;
                result += txCANFrame.Frame.Id.ToString("X3").Substring(0, 3);
            }


            result += txCANFrame.Frame.Dcl.ToString("X");

            for (int i = 0; i < txCANFrame.Frame.Dcl; i++)
            {
                result += ((byte) (txCANFrame.Frame.Data[i].Item)).ToString("X2");
            }

            result += "\r";

            return Encoding.UTF8.GetBytes(result);
        }

        /// <summary>
        /// Map CANFrame to Byte array
        /// </summary>
        /// <param name="canFrame"></param>
        /// <returns></returns>
        public static byte[] MapToByteArray(this CANFrame canFrame)
        {
            string result = string.Empty;

            if (canFrame.Is29Bit)
            {
                result += LawicelCmds.T;
                result += canFrame.Id.ToString("X8");
            }
            else
            {
                result += LawicelCmds.t;
                result += canFrame.Id.ToString("X3").Substring(0, 3);
            }


            result += canFrame.Dcl.ToString("X");

            for (int i = 0; i < canFrame.Dcl; i++)
            {
                result += ((byte) (canFrame.Data[i])).ToString("X2");
            }

            result += "\r";

            return Encoding.UTF8.GetBytes(result);
        }

        /// <summary>
        /// Convert ByteArray To HexString
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static string ByteArrayToHexString(byte[] bytes)
        {
            StringBuilder result = new StringBuilder(bytes.Length * 2);
            const string hexAlphabet = "0123456789ABCDEF";

            foreach (byte b in bytes)
            {
                result.Append(hexAlphabet[(int) (b >> 4)]);
                result.Append(hexAlphabet[(int) (b & 0xF)]);
            }

            return result.ToString();
        }

        /// <summary>
        /// Convert HexString To ByteArray
        /// </summary>
        /// <param name="hex"></param>
        /// <returns></returns>
        public static byte[] HexStringToByteArray(string hex)
        {
            byte[] bytes = new byte[hex.Length / 2];
            int[] hexValue = new int[]
            {
                0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F
            };

            for (int x = 0, i = 0; i < hex.Length; i += 2, x += 1)
            {
                bytes[x] = (byte) (hexValue[char.ToUpper(hex[i + 0]) - '0'] << 4 |
                                   hexValue[char.ToUpper(hex[i + 1]) - '0']);
            }

            return bytes;
        }
    }
}