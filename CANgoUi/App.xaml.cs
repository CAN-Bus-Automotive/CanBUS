﻿using System.Windows;
using System.Windows.Threading;
using NLog;

namespace CANgoUi
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        private readonly Logger _log = LogManager.GetCurrentClassLogger();

        public App()
        {
            InitializeComponent();
            _log.Info("---------------------------APP Started--------------------");
        }


        /// <summary>
        /// UnhandledExceptions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Application_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
#if !DEBUG
                _log.Fatal(e.Exception);
#endif
        }

        private void App_OnStartup(object sender, StartupEventArgs e)
        {
        }
    }
}