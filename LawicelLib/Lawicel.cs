using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LawicelLib
{
    public abstract class Lawicel : IDisposable
    {
        /* serial port */
        protected SerialPort sp;

        /* command mutex */
        SemaphoreSlim sem;

        /* list of supported commands */
        List<LawicelCmds> Commands;

        /* Receive complete lawicel message */
        public event EventHandler<string> OnReceiving;

        /* cancellation token source for reading task */
        private CancellationTokenSource _cts;

        /* cancellation token for reading task */
        private CancellationToken _ct;

        /* reading task state */
        protected bool _canHandleFrame = false;

        /* version */
        public string Version;

        /* product id */
        public ushort ProductID;

        private readonly StringBuilder _sb = new StringBuilder(100);

        /* constructor */
        public Lawicel()
        {
            Commands = new List<LawicelCmds>();
            /* initialize mutex */
            sem = new SemaphoreSlim(1);
        }

        /* destructor */
        ~Lawicel()
        {
            /* dispose of serial port */
            Dispose();
        }

        /* dispose implementation */
        public void Dispose()
        {
            /* close serial port */
            Close();
        }

        /// <summary>
        /// open serial port
        /// </summary>
        /// <param name="portName"></param>
        /// <param name="baudRate"></param>
        public virtual void Open(string portName, uint baudRate = 115200)
        {
            try
            {
                Debug.WriteLine($"Open port: {portName} - {baudRate}");
                /* initialize serial port */
                sp = new SerialPort(portName, (int) baudRate, Parity.Even, 8);
                //sp.DtrEnable = true;
                //sp.RtsEnable = true;
                /* open serial port */
                sp.Open();

                Debug.WriteLine($"DiscardInBuffer");
                /* discard buffers */
                sp.DiscardInBuffer();
                sp.DiscardOutBuffer();
            }
            catch (Exception e)
            {
                throw new LawicelException(e.Message);
            }
        }

        /// <summary>
        /// Close port and open again 
        /// </summary>
        /// <param name="portName"></param>
        /// <param name="baudRate"></param>
        public void TryOpen(string portName, uint baudRate = 115200)
        {
            Debug.WriteLine($"TryOpen port: {portName} - {baudRate}");
            Close();
            Open(portName, baudRate);
        }

        /// <summary>
        /// Close serial port
        /// </summary>
        public void Close()
        {
            /* close permitted? */
            if (sp != null && sp.IsOpen)
            {
                Debug.WriteLine($"Close port: {sp.PortName}");
                sp.Close();
            }
        }

        /// <summary>
        /// Open CAN chanel
        /// </summary>
        /// <returns></returns>
        public abstract Task OpenChanel();


        /// <summary>
        /// Close CAN chanel
        /// </summary>
        /// <returns></returns>
        public abstract Task CloseChanel();

        /// <summary>
        /// Get Device version
        /// </summary>
        /// <returns></returns>
        public virtual async Task<string> GetVersion()
        {
            Debug.WriteLine($"GetVersion");

            string cmd = LawicelCmds.v + "\r";
            byte[] bytes = Encoding.ASCII.GetBytes(cmd);

            await SerialWrite(bytes, 0, bytes.Length);
            var board = await SerialRead(1000);
            board = board.Replace("\r", "");

            cmd = LawicelCmds.V + "\r";
            bytes = Encoding.ASCII.GetBytes(cmd);

            await SerialWrite(bytes, 0, bytes.Length);
            var version = await SerialRead(1000);
            version = version.Replace("\r", "");

            cmd = LawicelCmds.N + "\r";
            bytes = Encoding.ASCII.GetBytes(cmd);

            await SerialWrite(bytes, 0, bytes.Length);
            var serial = await SerialRead(1000);
            serial = serial.Replace("\r", "");

            Debug.WriteLine($"Device response version: {board} - {version} - {serial}");
            return $"{board} - {version} - {serial}";
        }

        /// <summary>
        /// Set predefined CAN baudrate
        /// </summary>
        /// <param name="predefinedBaudrate">See lwicel doc</param>
        /// <returns></returns>
        public abstract Task SetDefaultBaudrate(string predefinedBaudrate);

        /// <summary>
        /// Set ON listen only mode
        /// </summary>
        /// <param name="state"></param>
        /// <returns></returns>
        public async Task SetListenOnly()
        {
            string cmd = LawicelCmds.L + "\r";
            byte[] bytes = Encoding.ASCII.GetBytes(cmd);

            await SerialWrite(bytes, 0, bytes.Length);
            var result = await SerialRead();

            if (result == LawicelResps.BELL.ToString())
            {
                throw new InvalidOperationException("Invalid command.");
            }
        }

        /// <summary>
        /// Enable-Disable timestamp in incoming frame
        /// </summary>
        /// <param name="state"></param>
        /// <returns></returns>
        public virtual async Task SetTimestamp(bool state)
        {
            /* value in Lawicel format */
            string lwValue = string.Empty;
            state.ToString();
            if (state == true)
            {
                lwValue = "1";
            }
            else
            {
                lwValue = "0";
            }

            string cmd = LawicelCmds.Z + lwValue + "\r";
            byte[] bytes = Encoding.ASCII.GetBytes(cmd);

            await SerialWrite(bytes, 0, bytes.Length);
            var result = await SerialRead();

            if (result == LawicelResps.BELL.ToString())
            {
                throw new InvalidOperationException("Invalid command.");
            }
        }

        /// <summary>
        /// Set Mask Filter
        /// </summary>
        /// <param name="mask"></param>
        /// <returns></returns>
        public async Task SetMaskFilter(string mask)
        {
            string cmd = LawicelCmds.m + mask + "\r";
            byte[] bytes = Encoding.ASCII.GetBytes(cmd);

            await SerialWrite(bytes, 0, bytes.Length);
            var result = await SerialRead();

            if (result == LawicelResps.BELL.ToString())
            {
                throw new InvalidOperationException("Invalid command.");
            }
        }

        /// <summary>
        /// Set Code Filter
        /// </summary>
        /// <param name="mask"></param>
        /// <returns></returns>
        public async Task SetCodeFilter(string code)
        {
            string cmd = LawicelCmds.M + code + "\r";
            byte[] bytes = Encoding.ASCII.GetBytes(cmd);

            await SerialWrite(bytes, 0, bytes.Length);
            var result = await SerialRead();

            if (result == LawicelResps.BELL.ToString())
            {
                throw new InvalidOperationException("Invalid command.");
            }
        }

        /// <summary>
        /// start reading task
        /// </summary>
        public void Start()
        {
            //Debug.WriteLine("Start");
            ///* discard buffers */
            //sp.DiscardInBuffer();
            //sp.DiscardOutBuffer();

            ///* change task state */
            //_canHandleFrame = false;
        }

        /// <summary>
        /// stop reading task
        /// </summary>
        public void Stop()
        {
            //Debug.WriteLine("Stop");

            ///* discard buffers */
            //sp.DiscardInBuffer();
            //sp.DiscardOutBuffer();

            ///* change task state */
            //_canHandleFrame = false;
        }

        /// <summary>
        /// Read CAN frame
        /// </summary>
        /// <returns></returns>
        public string ReadFrame()
        {
            string frame = null;

            if (_canHandleFrame)
            {
                frame = SerialContinuedRead();
                OnReceiving?.Invoke(this, frame);
            }
            else
            {
                //Debug.WriteLine("Error: canHandleFrame is false");
            }

            return frame;
        }

        /// <summary>
        /// Write Lawicel TX frame
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task SendTxFrame(byte[] data)
        {
            await sem.WaitAsync();
            await SerialWrite(data, 0, data.Length);
            sem.Release();
        }

        /// <summary>
        ///  write to serial port
        /// </summary>
        /// <param name="data"></param>
        /// <param name="offset"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        protected async Task SerialWrite(byte[] data, int offset, int count)
        {
            /* shorter name */
            var bs = sp.BaseStream;

            /* write operation */
            await bs.WriteAsync(data, offset, count);
        }


        /// <summary>
        /// Read lawicel string without timeout
        /// </summary>
        /// <returns></returns>
        private string SerialContinuedRead()
        {
            /* discard buffers */
            //sp.DiscardInBuffer();
           // sp.DiscardOutBuffer();
            /* shorter name */
            //var bs = sp.BaseStream;
            /* number of bytes read */
           // int br = 0;
            /* incoming buffer */
            byte[] data = new byte[1];

            _sb.Clear();

            /* read until all bytes are fetched from serial port */
            while (data[0] != LawicelResps.CR)
            {
                /* this try is for timeout handling */
                /* prepare task */
                sp.BaseStream.Read(data, 0, 1);
                _sb.Append(Convert.ToChar(data[0]));
            }

            return _sb.ToString();
        }

        /// <summary>
        ///  Read Lawicel packet from serial port 
        /// </summary>
        /// <param name="timeout"></param>
        /// <returns></returns>
        protected async Task<string> SerialRead(int timeout = 1000)
        {
            /* shorter name */
            var bs = sp.BaseStream;
            /* number of bytes read */
            int br = 0;
            /* incoming buffer */
            byte[] data = new byte[1];

            string response = string.Empty;

            /* read until all bytes are fetched from serial port */
            while (data[0] != LawicelResps.CR)
            {
                /* this try is for timeout handling */
                try
                {
                    /* prepare task */
                    await bs.ReadAsync(data, 0, 1).WithTimeout(timeout);
                    response += Convert.ToChar(data[0]);
                    /* got handling? */
                }
                catch (OperationCanceledException)
                {
                    /* rethrow */
                    throw new LawicelException("Timeout. No answer from device.");
                }
            }

            return response;
        }
    }
}