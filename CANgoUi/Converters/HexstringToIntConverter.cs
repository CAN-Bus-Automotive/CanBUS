﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace CANgoUi.Converters
{
    public class HexstringToIntConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            UInt32? intValue = value as UInt32?;

            var result = intValue?.ToString("X");

            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var hexString = value as string;

            if (string.IsNullOrWhiteSpace(hexString))
                return 0;

            var clearHexString = hexString.Replace(" ", string.Empty);

            UInt32? result = System.Convert.ToUInt32(clearHexString, 16);

            return result;
        }
    }
}
