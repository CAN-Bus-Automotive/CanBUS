﻿using System.Globalization;
using CANgoUi.Core;
using System.Windows;
using System.Windows.Input;

namespace CANgoUi.Views
{
    /// <summary>
    /// Interaction logic for NewMessageView.xaml
    /// </summary>
    public partial class NewMessageView : Window, IClosable
    {
        public NewMessageView()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void OnPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int hexNumber;
            e.Handled = !int.TryParse(e.Text, NumberStyles.HexNumber, CultureInfo.CurrentCulture, out hexNumber);
        }
    }
}