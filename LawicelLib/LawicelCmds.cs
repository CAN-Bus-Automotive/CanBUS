﻿namespace LawicelLib
{
    /// <summary>
    /// LAWICEL CANUSB ASCII Commands.
    /// See more in: http://www.can232.com/docs/canusb_manual.pdf
    /// </summary>
    public class LawicelCmds
    {
        /// <summary>
        /// Setup with standard CAN bit-rates where n is 0-8.
        /// This command is only active if the CAN channel is closed
        /// </summary>
        /// <returns> CR (Ascii 13) for OK or BELL (Ascii 7) for ERROR.</returns>
        public const string S = "S";

        /// <summary>
        /// Setup with BTR0/BTR1 CAN bit-rates where xx and yy is a hex
        /// value.This command is only active if the CAN channel is closed.
        /// </summary>
        /// <returns> CR (Ascii 13) for OK or BELL (Ascii 7) for ERROR.</returns>
        public const string s = "s";

        /// <summary>
        /// Open the CAN channel.
        /// This command is only active if the CAN channel is closed and
        /// has been set up prior with either the S or s command(i.e.initiated).
        /// </summary>
        /// <returns> CR (Ascii 13) for OK or BELL (Ascii 7) for ERROR.</returns>
        public const string O = "O";

        /// <summary>
        /// Close the CAN channel.
        /// This command is only active if the CAN channel is open.
        /// </summary>
        /// <returns> CR (Ascii 13) for OK or BELL (Ascii 7) for ERROR.</returns>
        public const string C = "C";

        /// <summary>
        /// Transmit a standard (11bit) CAN frame.
        /// This command is only active if the CAN channel is open.
        /// </summary>
        /// <returns> CR (Ascii 13) for OK or BELL (Ascii 7) for ERROR.</returns>
        public const string t = "t";

        /// <summary>
        /// Transmit an extended (29bit) CAN frame.
        /// This command is only active if the CAN channel is open.
        /// </summary>
        /// <returns> CR (Ascii 13) for OK or BELL (Ascii 7) for ERROR.</returns>
        public const string T = "T";

        /// <summary>
        /// Transmit an standard RTR (11bit) CAN frame.
        /// This command is only active if the CANUSB is open in normal mode.
        /// </summary>
        /// <returns> CR (Ascii 13) for OK or BELL (Ascii 7) for ERROR.</returns>
        public const string r = "r";

        /// <summary>
        /// Transmit an extended RTR (29bit) CAN frame.
        /// This command is only active if the CANUSB is open in normal mode.
        /// </summary>
        /// <returns> CR (Ascii 13) for OK or BELL (Ascii 7) for ERROR.</returns>
        public const string R = "R";

        /// <summary>
        /// Read Status Flags.
        /// This command is only active if the CAN channel is open.
        /// </summary>
        /// <returns> 
        /// An F with 2 bytes BCD hex value plus CR (Ascii 13)
        /// for OK.If CAN channel isn’t open it returns BELL
        /// (Ascii 7). This command also clear the RED Error
        /// LED.See availible errors below.E.g.F01[CR]
        ///     Bit 0 CAN receive FIFO queue full
        ///     Bit 1 CAN transmit FIFO queue full
        ///     Bit 2 Error warning(EI), see SJA1000 datasheet
        ///     Bit 3 Data Overrun(DOI), see SJA1000 datasheet
        ///     Bit 4 Not used.
        ///     Bit 5 Error Passive (EPI), see SJA1000 datasheet
        ///     Bit 6 Arbitration Lost (ALI), see SJA1000 datasheet*
        ///     Bit 7 Bus Error (BEI), see SJA1000 datasheet**
        /// </returns>
        public const string F = "F";

        /// <summary>
        /// Sets Acceptance Code Register (ACn Register of SJA1000).
        /// This command is only active if the CAN channel is initiated and not opened.
        /// </summary>
        /// <returns> CR (Ascii 13) for OK or BELL (Ascii 7) for ERROR.</returns>
        public const string M = "M";

        /// <summary>
        /// Sets Acceptance Mask Register (AMn Register of SJA1000).
        /// This command is only active if the CAN channel is initiated and not opened.
        /// </summary>
        /// <returns> CR (Ascii 13) for OK or BELL (Ascii 7) for ERROR.</returns>
        public const string m = "m";

        /// <summary>
        /// Get Version number of both CANUSB hardware and software
        /// This command is active always.
        /// </summary>
        /// <returns>
        /// V and a 2 bytes BCD value for hardware version and
        /// a 2 byte BCD value for software version plus
        /// CR(Ascii 13) for OK.E.g.V1013[CR]
        /// </returns>
        public const string V = "V";

        /// <summary>
        /// Get Board Version number of both CANUSB hardware and software
        /// This command is active always.
        /// </summary>
        /// <returns>
        /// V and a 2 bytes BCD value for hardware version and
        /// a 2 byte BCD value for software version plus
        /// CR(Ascii 13) for OK.E.g.V1013[CR]
        /// </returns>
        public const string v = "v";

        /// <summary>
        /// Get Serial number of the CANUSB.
        /// This command is active always.
        /// </summary>
        /// <returns>
        /// N and a 4 bytes value for serial number plus
        /// CR(Ascii 13) for OK.E.g.NA123[CR]
        /// Note that the serial number can have both numerical
        /// and alfa numerical values in it.The serial number is
        /// also printed on the CANUSB for a quick reference,
        /// but could e.g.be used in a program to identify a
        /// CANUSB so the program know that it is set up in the
        /// correct way(for parameters saved in EEPROM).
        /// </returns>
        public const string N = "N";

        /// <summary>
        /// Sets Time Stamp ON/OFF for received frames only.
        /// This command is only active if the CAN channel is closed
        /// </summary
        /// <remarks>
        /// If the Time Stamp is ON, the incomming frames looks like this:
        /// t100211334D67[CR] (a standard frame with ID = 0x100 & 2 bytes)
        /// Note the last 4 bytes 0x4D67, which is a Time Stamp for this
        /// specific message in milliseconds(and of course in hex).
        /// </remarks>
        /// <returns> CR (Ascii 13) for OK or BELL (Ascii 7) for ERROR.</returns>
        public const string Z = "Z";

        /// <summary>
        /// Listen only.
        /// This command is only active if the CAN channel is closed
        /// </summary
        /// <remarks>
        /// If the Time Stamp is ON, the incomming frames looks like this:
        /// t100211334D67[CR] (a standard frame with ID = 0x100 & 2 bytes)
        /// Note the last 4 bytes 0x4D67, which is a Time Stamp for this
        /// specific message in milliseconds(and of course in hex).
        /// </remarks>
        /// <returns> CR (Ascii 13) for OK or BELL (Ascii 7) for ERROR.</returns>
        public const string L = "L";
    }
}
