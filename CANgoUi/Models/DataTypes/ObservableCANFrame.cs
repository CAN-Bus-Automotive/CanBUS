﻿using System;
using GalaSoft.MvvmLight;

namespace CANgoUi.Models.DataTypes
{
    public class ObservableCANFrame : ViewModelBase, ICloneable
    {
        public uint Id { get; set; }

        public int Dcl { get; set; }

        public ObservableItem[] Data { get; set; }

        public bool Is29Bits { get; set; }

        public int Count { get; set; }

        public int SelectedByteIndex { get; set; }

        public uint TimeStamp { get; set; }

        public uint Period { get; set; }

        public string Comment { get; set; }


        public void ResetIndex()
        {
            SelectedByteIndex = 10;
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}