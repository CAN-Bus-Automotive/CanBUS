﻿using GalaSoft.MvvmLight;

namespace CANgoUi.Models.DataTypes
{
    public class ObservableItem : ViewModelBase
    {
        public byte? Item { get; set; }

        public bool UseIncrement { get; set; }

        public bool IsVisible { get; set; }

        public bool Changed { get; set; }

        public byte? TempItem { get; set; }
    }
}