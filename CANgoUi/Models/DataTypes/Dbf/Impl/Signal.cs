﻿using System.Collections;
using System.Collections.Generic;
using CANFrameLib;
using CANgoUi.Helpers;
using GalaSoft.MvvmLight;

namespace CANgoUi.Models.DataTypes.Dbf.Impl
{
    public class Signal : ViewModelBase, ISignal
    {
        // Public
        public string Name { get; set; }
        public uint ByteIndex { get; set; }
        public uint StartBit { get; set; }
        public uint BitCount { get; set; }
        public double Factor { get; set; }
        public uint Offset { get; set; }
        public string Unit { get; set; }
        public double Value { get; set; }
        public SignalType SignalType { get; set; }
        public Endianness Endianness { get; set; } = Endianness.BigEndian;
        public List<double> ValueLog { get; set; } = new List<double>();
        
        // Private
        private readonly long[] _tmpArray = new long[1];

        // Constructor for XML serialisation
        public Signal()
        {
        }

        // Constructor
        public Signal(uint startBit, uint bitCount, string name, uint byteIndex, double factor, uint offset,
            SignalType signalType, Endianness endianness, string unit)
        {
            StartBit = startBit;
            BitCount = bitCount;
            Name = name;
            ByteIndex = byteIndex;
            Factor = factor;
            Offset = offset;
            SignalType = signalType;
            Unit = unit;
        }

        public void Update(CANFrame canFrame)
        {
            var dataBits = new BitArray(canFrame.Data);

            byte[] tmpArr = ConvertHelpers.BitArrayToByteArray(GetSelectedBits(dataBits));

            if (Endianness == Endianness.BigEndian)
            {
                _tmpArray[0] = ConvertHelpers.ToBigEndian(tmpArr, 0, tmpArr.Length);
            }
            else
            {
                _tmpArray[0] = ConvertHelpers.ToLittleEndian(tmpArr, 0, tmpArr.Length);
            }

            Value = _tmpArray[0] * Factor + Offset;
            ValueLog.Add(Value);
        }

        public BitArray GetSelectedBits(BitArray bitArray)
        {
            var result = new BitArray((int) BitCount);

            for (int i = (int) (StartBit + (ByteIndex * 8)), j = 0; j < BitCount; i++, j++)
            {
                result[j] = bitArray[i];
            }

            return result;
        }
    }
}