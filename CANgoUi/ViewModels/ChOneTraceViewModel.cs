﻿using System.Windows;
using CANgoUi.Mappers;
using CANgoUi.Services;
using CANgoUi.Views;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace CANgoUi.ViewModels
{
    public class ChOneTraceViewModel : ViewModelBase
    {
        // Private
        private readonly MessageService _messageService;
        private readonly FrameBufferService _bufferService;
        private readonly TransmitService _transmitService;

        // Commands
        public RelayCommand ClearBuffersCmd => new RelayCommand(() => _bufferService.ClearBuffers());
        public RelayCommand CopyToTxListCmd => new RelayCommand(CopyToTxList);
        public RelayCommand ShowIdStoryCmd => new RelayCommand(ShowIdStory);
        public RelayCommand ShowChartCmd => new RelayCommand(ShowChart);
        public RelayCommand SortByIdCmd => new RelayCommand(SortById);
        public RelayCommand UpdateSelectionCmd => new RelayCommand(() => _bufferService.UpdateSelection());

        // Constructor
        public ChOneTraceViewModel(MessageService messageService, FrameBufferService bufferService,
            TransmitService transmitService)
        {
            _messageService = messageService;
            _bufferService = bufferService;
            _transmitService = transmitService;
        }

        /// <summary>
        /// Copy selected frame to TX list
        /// </summary>
        public void CopyToTxList()
        {
            if (_bufferService.SelectedItem != null)
            {
                var txFrame = _bufferService.SelectedItem.Value.MapToTxCANFrame();
                _transmitService.TxFrames.Add(txFrame);
            }
        }

        /// <summary>
        /// Show filtered by ID messages
        /// </summary>
        private void ShowIdStory()
        {
            if (_bufferService.SelectedItem != null)
            {
                IdStoryView aboutView = new IdStoryView();
                aboutView.Owner = Application.Current.MainWindow;
                aboutView.WindowStartupLocation = WindowStartupLocation.CenterOwner;

                aboutView.Show();
            }
        }

        private void ShowChart()
        {
            if (_bufferService.SelectedItem != null)
            {
                ChartView chartView = new ChartView();
                chartView.Owner = Application.Current.MainWindow;
                chartView.WindowStartupLocation = WindowStartupLocation.CenterOwner;

                chartView.Show();
            }
        }

        public void SortById()
        {
            _bufferService.ObservableCanFrames.Sort();
        }
    }
}