﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using System.Windows.Controls;
using CANFrameLib;
using CANgoUi.Helpers;
using CANgoUi.Mappers;
using CANgoUi.Models.DataTypes;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Threading;
using NLog;

namespace CANgoUi.Services
{
    public class FrameBufferService : ViewModelBase
    {
        // Public
        public ObservableRangeCollection<CANFrame> CANFrames { get; set; }
        public ObservablePairCollection<uint, ObservableCANFrame> ObservableCanFrames { get; set; }
        public BitByte SelectedByte { get; set; }
        public Pair<uint, ObservableCANFrame> SelectedItem { get; set; }
        public bool IsBitViewVisibility { get; set; }

        public DataGridCellInfo CellInfo
        {
            get { return _cellInfo; }
            set
            {
                Set(() => CellInfo, ref _cellInfo, value);
                foreach (var f in ObservableCanFrames)
                {
                    f.Value.ResetIndex();
                }
            }
        }

        // public ICollectionViewLiveShaping LiveItems { get; set; }

        // Private
        private readonly Logger _log = LogManager.GetCurrentClassLogger();
        private readonly ConcurrentQueue<CANFrame> _framesQueue;
        private readonly TraceService _traceService = SimpleIoc.Default.GetInstance<TraceService>();
        private int _newFramesCount = 0;
        private const int DefaultInterval = 200;
        private ObservableCANFrame ObsFrame { get; set; }
        private CANFrameFactory _frameFactory = new CANFrameFactory();
        private DataGridCellInfo _cellInfo;

        //  Constructor
        public FrameBufferService()
        {
            DispatcherHelper.Initialize();

            CANFrames = new ObservableRangeCollection<CANFrame>();
            ObservableCanFrames = new ObservablePairCollection<uint, ObservableCANFrame>();
            SelectedByte = new BitByte();
            _framesQueue = new ConcurrentQueue<CANFrame>();
            //LiveItems = (ICollectionViewLiveShaping)CollectionViewSource.GetDefaultView(ObservableCanFrames);
            // LiveItems.IsLiveSorting = true;

            var timer = new Timer(DefaultInterval);
            timer.AutoReset = true;
            timer.Elapsed += TimerElapsed;
            timer.Start();

            if (IsInDesignModeStatic)
            {
                var obsFrame1 = _frameFactory.GetObservableCANFrame();
                var obsFrame2 = _frameFactory.GetObservableCANFrame();
                ObservableCanFrames.Add(obsFrame1.Id, obsFrame1);
                ObservableCanFrames.Add(obsFrame2.Id, obsFrame2);
            }
        }

        private void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            int iterator = _framesQueue.Count;
            _newFramesCount = 0;

            CANFrame frame;
            for (int i = 0; i < iterator; i++)
            {
                _framesQueue.TryDequeue(out frame);
                InsertFrame(frame);
            }

            // Update UI
            UpdateObservableList();
        }

        public void AddToQueue(CANFrame frame)
        {
            _framesQueue.Enqueue(frame);
            _newFramesCount++;
        }

        /// <summary>
        /// Insert new CANFrame to buffer
        /// </summary>
        /// <param name="frame"></param>
        public void InsertFrame(CANFrame frame)
        {
            if (frame?.Data != null)
            {
                DispatcherHelper.RunAsync(() =>
                {
                    CANFrames.Add(frame);
                    InsertToObservableList(frame);
                    if (_traceService.IsRec)
                    {
                        _traceService.TraceBuffer.Add(frame);
                    }
                });
            }
        }

        /// <summary>
        /// Useful method for fast add items from other source (load a saved data from a file)
        /// </summary>
        /// <param name="buffer"></param>
        public void InsertFromBuffer(FrameBufferService buffer)
        {
            DispatcherHelper.RunAsync(() =>
            {
                CANFrames.AddRange(buffer.CANFrames);
                foreach (var frame in buffer.ObservableCanFrames)
                {
                    this.ObservableCanFrames.Add(frame);
                }
            });
        }

        /// <summary>
        /// Insert new CANFrame to ObservableList
        /// </summary>
        /// <param name="frame"></param>
        private void InsertToObservableList(CANFrame frame)
        {
            ObsFrame = frame.MapToObservableCanFrame();

            if (ObservableCanFrames.All(p => p.Key != ObsFrame.Id))
            {
                ObservableCanFrames.Add(ObsFrame.Id, ObsFrame);
            }
            else
            {
                var tempFrame = ObservableCanFrames.First(p => p.Key == ObsFrame.Id).Value;

                //TODO Add Id filter check
                tempFrame.Period = ObsFrame.TimeStamp - tempFrame.TimeStamp;
                tempFrame.TimeStamp = ObsFrame.TimeStamp;

                if (tempFrame.Dcl != ObsFrame.Dcl)
                {
                    tempFrame.Dcl = ObsFrame.Dcl;
                }

                if (tempFrame.Id != ObsFrame.Id)
                {
                    tempFrame.Id = ObsFrame.Id;
                }

                for (int i = 0; i < ObsFrame.Data.Length; i++)
                {
                    if (tempFrame.Data[i].Item != ObsFrame.Data[i].Item)
                    {
                        // Mark data byte as changed (it will updated in UpdateObservableList method)
                        tempFrame.Data[i].Changed = true;
                        tempFrame.Data[i].TempItem = ObsFrame.Data[i].Item; // Member updated value
                    }

                    if (tempFrame.SelectedByteIndex == i)
                    {
                        var item = tempFrame.Data[i].Item;
                        if (item != null)
                            SelectedByte.SetBitByte(item.Value);
                    }
                }

                tempFrame.Count++;
            }
        }

        /// <summary>
        /// Update ObservableList and change data in UI
        /// </summary>
        public void UpdateObservableList()
        {
            foreach (var obsFrame in ObservableCanFrames)
            {
                for (int i = 0; i < obsFrame.Value.Data.Length; i++)
                {
                    if (obsFrame.Value.Data[i].Changed)
                    {
                        obsFrame.Value.Data[i].Changed = false;
                        obsFrame.Value.Data[i].Item = obsFrame.Value.Data[i].TempItem;
                    }
                }
            }
        }

        /// <summary>
        /// Insert new list of CANFrames to ObservableList
        /// </summary>
        /// <param name="frames"></param>
        public void InsertToObservableList(List<CANFrame> frames)
        {
            foreach (var frame in frames)
            {
                ObsFrame = frame.MapToObservableCanFrame();

                if (ObservableCanFrames.All(p => p.Key != ObsFrame.Id))
                {
                    ObservableCanFrames.Add(ObsFrame.Id, ObsFrame);
                }
                else
                {
                    var tempFrame = ObservableCanFrames.First(p => p.Key == ObsFrame.Id).Value;
                    tempFrame.Count++;
                }
            }
        }

        /// <summary>
        /// Clear all buffers
        /// </summary>
        public void ClearBuffers()
        {
            CANFrames.Clear();
            ObservableCanFrames.Clear();
        }

        /// <summary>
        /// Get frames by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<CANFrame> FilterById(uint id)
        {
            return CANFrames.Where(f => f.Id == id).ToList();
        }

        /// <summary>
        /// Update selected item in data grid
        /// </summary>
        public void UpdateSelection()
        {
            try
            {
                if (SelectedItem != null)
                {
                    SelectedItem.Value.SelectedByteIndex =
                        CellInfo.Column.DisplayIndex - 3; // 3 - cell offset, data cell begins from 3 cells offset
                    var item = SelectedItem.Value.Data[SelectedItem.Value.SelectedByteIndex].Item;

                    if (item != null)
                        SelectedByte.SetBitByte((byte) item);
                }
            }
            catch (IndexOutOfRangeException)
            {
                //_log.Error(e);
            }
        }
    }
}