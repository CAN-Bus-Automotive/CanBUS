﻿namespace CANgoUi.Models.DataTypes.Dbf.Impl.SignalConverters
{
    public static class SignalConvFactory
    {
        public static ISignalConverter GetIntegerConverter()
        {
            return new IntegerConverter() { Name = "Integer" };
        }

        public static ISignalConverter GetBooleanConverter()
        {
            return new BooleanConverter() { Name = "Boolean" };
        }

    }
}
