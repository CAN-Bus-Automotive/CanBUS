﻿using System;
using System.Collections.Generic;
using System.Management;
using System.Text.RegularExpressions;

namespace CANgoUi.Services
{
    public class SerialPortService
    {
        private readonly Regex _regex = new Regex("COM");

        /// <summary>
        /// Return all aviable serial ports with PID info.
        /// </summary>
        /// <returns></returns>
        public List<string> GetPortList()
        {
            List<string> portList = new List<string>();
            string query = "SELECT * FROM Win32_PnPEntity WHERE ConfigManagerErrorCode = 0";

            var searcher = new ManagementObjectSearcher(query);
            foreach (ManagementObject service in searcher.Get())
            {
                try
                {
                    if (_regex.IsMatch(service["Name"].ToString()))
                    {
                        portList.Add(service["Name"].ToString());
                    }
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.Write(e.Message);
                }
            }

            return portList;
        }


        /// <summary>
        /// Set port name. Incapsulate "PORT NAME" of line.
        /// </summary>
        /// <param name="longPortName"></param>
        /// <returns>Port nmae string with cendor info</returns>
        public string SetPortName(string longPortName)
        {
            string shortPortName = "";
            string pattern = @"COM\d+"; // Паттерн для выделения имени COM порта из строки
            try
            {
                MatchCollection matches = Regex.Matches(longPortName, pattern); // Процесс выделения имени COM порта из строки
                foreach (Match match in matches)
                    shortPortName = match.Value;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.Write(e.Message);
            }

            return shortPortName;
        }

    }

}
