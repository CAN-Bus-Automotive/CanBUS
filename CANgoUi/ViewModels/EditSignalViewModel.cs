﻿using System.Linq;
using CANgoUi.Core;
using CANgoUi.Models.DataTypes;
using CANgoUi.Models.DataTypes.Dbf;
using CANgoUi.Models.DataTypes.Dbf.Impl;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace CANgoUi.ViewModels
{
    public class EditSignalViewModel : ViewModelBase
    {
        private readonly DbfMessage _dbfMessage;
        private readonly Signal _signal;

        // Public
        public string Name { get; set; }
        public uint ByteIndex { get; set; }
        public uint StartBit { get; set; }
        public uint BitCount { get; set; }
        public double Factor { get; set; }
        public uint Offset { get; set; }
        public string Unit { get; set; }
        public SignalType SelectedSignalType { get; set; }
        public Endianness SelectedEndianness { get; set; }


        // Commands
        public RelayCommand<IClosable> UpdateSignalCmd => new RelayCommand<IClosable>(UpdateSignal);


        // Constructor
        public EditSignalViewModel(Signal signal, DbfMessage dbfMessage)
        {
            _dbfMessage = dbfMessage;
            _signal = signal;

            Name = signal.Name;
            ByteIndex = signal.ByteIndex;
            StartBit = signal.StartBit;
            BitCount = signal.BitCount;
            Factor = signal.Factor;
            Offset = signal.Offset;
            Unit = signal.Unit;
            SelectedSignalType = signal.SignalType;
            SelectedEndianness = signal.Endianness;
        }

        /// <summary>
        /// Update selected signal
        /// </summary>
        public void UpdateSignal(IClosable window)
        {
            var firstOrDefault = _dbfMessage.Signals.FirstOrDefault(s => s == _signal);

            if (firstOrDefault != null)
            {
                firstOrDefault.Name = Name;
                firstOrDefault.ByteIndex = ByteIndex;
                firstOrDefault.StartBit = StartBit;
                firstOrDefault.BitCount = BitCount;
                firstOrDefault.Factor = Factor;
                firstOrDefault.Offset = Offset;
                firstOrDefault.Unit = Unit;
                firstOrDefault.SignalType = SelectedSignalType;
                firstOrDefault.Endianness = SelectedEndianness;
            }

            window.Close();
        }
    }
}