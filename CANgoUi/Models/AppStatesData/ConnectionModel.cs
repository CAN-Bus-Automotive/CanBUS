﻿namespace CANgoUi.Models.AppStatesData
{
    public class ConnectionModel
    {
        public bool RtsHsMode { get; set; }
        public bool ListenOnlyMode { get; set; }
        public bool TimeStampMode { get; set; } = true;
        public string SelectedPort { get; set; } = "COM1";
        public string SelectedBaud { get; set; } = "500";
    }
}
