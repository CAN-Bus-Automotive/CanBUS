﻿using System.ComponentModel;

namespace CANFrameLib
{
    public enum CanFrameDirections
    {
        [Description("=>")] RX = 1,
        [Description("<=")] TX
    }
}