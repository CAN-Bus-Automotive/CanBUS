﻿using System.Diagnostics;
using System.Reflection;
using System.Windows;

namespace CANgoUi.Views
{
    /// <summary>
    /// Interaction logic for AboutView.xaml
    /// </summary>
    public partial class AboutView : Window
    {
        public AboutView()
        {
            InitializeComponent();

            Version.Content = FileVersionInfo.GetVersionInfo
                (Assembly.GetExecutingAssembly().Location).ProductVersion;
            FullVersion.Text = FileVersionInfo.GetVersionInfo
                (Assembly.GetExecutingAssembly().Location).ToString();
        }

        private void Hyperlink_RequestNavigate(object sender, System.Windows.Navigation.RequestNavigateEventArgs e)
        {
            Process.Start(e.Uri.AbsoluteUri);
            e.Handled = true;
        }
    }
}