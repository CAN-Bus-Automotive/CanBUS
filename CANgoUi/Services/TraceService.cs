﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using CANFrameLib;
using CANgoUi.Mappers;
using CANgoUi.Models.DataTypes;
using GalaSoft.MvvmLight;
using LawicelLib;

namespace CANgoUi.Services
{
    public class TraceService : ViewModelBase
    {
        // Public
        public bool IsRec { get; set; }
        public bool IsRunning { get; set; }
        public bool CanRec { get; set; } = true;
        public bool Looped { get; set; } = false;
        public ObservableRangeCollection<CANFrame> TraceBuffer { get; set; }
        public int CurrentItem { get; set; }

        // Private
        private readonly Lawicel _lawicel;
        private BackgroundWorker _worker;

        // Constructor
        public TraceService(Lawicel lawicel)
        {
            _lawicel = lawicel;
            TraceBuffer = new ObservableRangeCollection<CANFrame>();
        }

        /// <summary>
        /// Clear trace buffer
        /// </summary>
        public void ClearTraceBuffer()
        {
            TraceBuffer.Clear();
        }

        /// <summary>
        /// Start buffer repaly
        /// </summary>
        public void RunReplay()
        {
            _worker = new BackgroundWorker();
            _worker.WorkerSupportsCancellation = true;
            _worker.DoWork += Worker_DoWorkAsync;
            _worker.RunWorkerAsync();
        }

        /// <summary>
        /// Stop buffer replay
        /// </summary>
        public void StopReplay()
        {
            IsRunning = false;
            CanRec = true;
        }

        /// <summary>
        /// Reset current item
        /// </summary>
        public void Reset()
        {
            CurrentItem = 0;
        }

        /// <summary>
        /// Make one step througth buffer
        /// </summary>
        public async void StepOne()
        {
            if (CurrentItem < TraceBuffer.Count)
            {
                await _lawicel.SendTxFrame(TraceBuffer[CurrentItem].MapToByteArray());
                CurrentItem++;
            }
        }

        private async void Worker_DoWorkAsync(object sender, DoWorkEventArgs e)
        {
            IsRunning = true;
            CanRec = false;

            for (CurrentItem = CurrentItem; CurrentItem < TraceBuffer.Count - 1; CurrentItem++)
            {
                var millisecondsDelay = (int) (TraceBuffer[CurrentItem + 1].SystemTimeStamp -
                                               TraceBuffer[CurrentItem].SystemTimeStamp);

                await _lawicel.SendTxFrame(TraceBuffer[CurrentItem].MapToByteArray());
                Thread.Sleep(millisecondsDelay);

                if (!IsRunning)
                {
                    break;
                }
            }

            if (CurrentItem == TraceBuffer.Count - 1)
            {
                IsRunning = false;
                CurrentItem = 0;
                CanRec = true;
                if (Looped)
                {
                    RunReplay();
                }
            }
        }
        
    }
}