﻿using CANgoUi.Core;
using CANgoUi.Models.DataTypes;
using CANgoUi.Models.DataTypes.Dbf;
using CANgoUi.Models.DataTypes.Dbf.Impl;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace CANgoUi.ViewModels
{
    public class NewSignalViewModel : ViewModelBase
    {
        // Private
        private readonly DbfMessage _dbfMessage;

        // Public
        public string Name { get; set; } = "New signal";
        public uint ByteIndex { get; set; }
        public uint StartBit { get; set; }
        public uint BitCount { get; set; } = 1;
        public double Factor { get; set; } = 1;
        public uint Offset { get; set; }
        public string Unit { get; set; } = "None";
        public SignalType SelectedSignalType { get; set; } = SignalType.Int32;
        public Endianness SelectedEndianness { get; set; } = Endianness.BigEndian;
        public uint MaximumBits { get; set; }
        public uint NextSignalStartBit { get; set; }

        // Commands
        public RelayCommand<IClosable> AddNewSignalCmd => new RelayCommand<IClosable>(AddNewSignal);

        // Constructor
        public NewSignalViewModel(DbfMessage dbfMessage)
        {
            _dbfMessage = dbfMessage;
        }

        public void UpdateConstraints()
        {
            foreach (var signal in _dbfMessage.Signals)
            {
                if (signal.ByteIndex * 8 + StartBit > StartBit + BitCount &&
                    signal.ByteIndex * 8 + StartBit < NextSignalStartBit)
                {
                    NextSignalStartBit = signal.ByteIndex * 8 + StartBit;
                }
            }

            MaximumBits = ByteIndex * 8 + StartBit + BitCount - NextSignalStartBit;
        }

        public void AddNewSignal(IClosable window)
        {
            Signal signal = new Signal(StartBit, BitCount, Name, ByteIndex, Factor, Offset, SelectedSignalType,
                SelectedEndianness, Unit);

            _dbfMessage.Signals.Add(signal);

            window.Close();
        }
    }
}