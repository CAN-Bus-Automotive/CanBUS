using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CANFrameLib;
using CANgoUi.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;

namespace CANgoUi.ViewModels
{
    public class ChartViewModel : ViewModelBase
    {
        // Private
        private readonly FrameBufferService _bufferService;
        private List<OxyColor> SetColors { get; set; }
        private LinearAxis xAxis;
        private LinearAxis yAxis;

        // Public
        public PlotModel ChartModel { get; set; }
        public string Title { get; set; }
        public List<DataPoint> Points { get; private set; }
        public List<DataPoint>[] ChartPoints { get; private set; }
        public int InvalidateFlag { get; set; }

        // Commands
        public RelayCommand UpdateChartCmd => new RelayCommand(UpdateChart);
        public RelayCommand HideAllChartsCmd => new RelayCommand(HideAllCharts);
        public RelayCommand ShowAllChartsCmd => new RelayCommand(ShowAllCharts);

        // Constructor
        public ChartViewModel(FrameBufferService bufferService)
        {
            _bufferService = bufferService;

            Points = new List<DataPoint>();
            ChartPoints = new List<DataPoint>[10];
            ChartModel = new PlotModel();

            SetColors = new List<OxyColor>
            {
                OxyColors.Red,
                OxyColors.Orange,
                OxyColors.Yellow,
                OxyColors.Green,
                OxyColors.Blue,
                OxyColors.Indigo,
                OxyColors.Violet,
                OxyColors.Azure
            };

            ViewInit();

            var frames = _bufferService.FilterById(_bufferService.SelectedItem.Value.Id);
            Title = $"Chart for Id: {_bufferService.SelectedItem.Value.Id:X}";

            MakeChart(frames);
        }

        private void ViewInit()
        {
            ChartModel.LegendTitle = "Legend";
            ChartModel.LegendOrientation = LegendOrientation.Vertical;
            ChartModel.LegendPlacement = LegendPlacement.Inside;
            ChartModel.LegendPosition = LegendPosition.TopRight;
            ChartModel.LegendBackground = OxyColor.FromAColor(200, OxyColors.White);
            ChartModel.LegendBorder = OxyColors.Black;

            ChartModel.PlotAreaBorderColor = OxyColors.LightGray;


            xAxis = new LinearAxis()
            {
                ExtraGridlineColor = OxyColors.LightGray,
                TextColor = OxyColors.LightGray,
                MajorGridlineColor = OxyColors.DarkSlateGray,
                MinorGridlineColor = OxyColors.DarkSlateGray,
                MajorGridlineStyle = LineStyle.Dot,
                MajorGridlineThickness = 1,
                Title = "Value",
                TitleColor = OxyColors.LightGray,
                Position = AxisPosition.Left,
                LabelFormatter = (d) => $"0x{Convert.ToInt32(d):X}",
                Minimum = -10,
                Maximum = 300
            };
            yAxis = new LinearAxis()
            {
                Title = "Time",
                TitleColor = OxyColors.LightGray,
                ExtraGridlineColor = OxyColors.LightGray,
                TextColor = OxyColors.LightGray,
                MajorGridlineColor = OxyColors.DarkSlateGray,
                MinorGridlineColor = OxyColors.DarkSlateGray,
                MajorGridlineStyle = LineStyle.Dot,
                MajorGridlineThickness = 1,
                Position = AxisPosition.Bottom
            };
            ChartModel.Axes.Add(xAxis);
            ChartModel.Axes.Add(yAxis);
        }

        public async void MakeChart(IEnumerable<CANFrame> frames)
        {
            await Task.Run(() =>
            {
                int timeLine = 0;
                foreach (var f in frames)
                {
                    for (int i = 0; i < f.Dcl; i++)
                    {
                        if (ChartModel.Series.Count < (i + 1)) // Add new chart to chart series
                        {
                            ChartModel.Series.Add(new LineSeries()
                                {Title = $"B{i}", StrokeThickness = 1.3, Color = SetColors[i]});
                        }

                        LineSeries a = (LineSeries) ChartModel.Series[i];
                        a.Points.Add(new DataPoint(timeLine, Convert.ToDouble(f.Data[i])));
                    }

                    timeLine++;
                }

                yAxis.Maximum = timeLine;
            });
        }

        public void UpdateChart()
        {
            ChartModel.InvalidatePlot(true);
        }

        public void HideAllCharts()
        {
            foreach (var series in ChartModel.Series)
            {
                series.IsVisible = false;
            }

            ChartModel.InvalidatePlot(true);
        }

        public void ShowAllCharts()
        {
            foreach (var series in ChartModel.Series)
            {
                series.IsVisible = true;
            }

            ChartModel.InvalidatePlot(true);
        }
    }
}