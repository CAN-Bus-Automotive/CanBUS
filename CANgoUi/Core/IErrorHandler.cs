﻿using System;

namespace CANgoUi.Core
{
    public interface IErrorHandler
    {
        void HandleError(Exception ex);
    }
}