using System.ComponentModel;

namespace CANgoUi.Models.DataTypes.Dbf
{
    public enum SignalType
    {
        [Description("Int16")]
        Int16,
        [Description("Int32")]
        Int32,
        [Description("Int64")]
        Int64,
        [Description("Boolean")]
        Boolean
        
    }
}