﻿using System;

namespace LawicelEmulator
{
    class Program
    {
        static void Main(string[] args)
        {
            EmulatorConfig cfg = new EmulatorConfig();

            if (args.Length != 2)
            {
                Console.WriteLine("Wrong arguments count.");
                return;
            }

            cfg.PortName = args[0];
            cfg.UniqFramesCount = Convert.ToInt32(args[1]);

            Emulator emulator = new Emulator(cfg);

            emulator.Emulate();

            Console.ReadKey();
        }
    }
}
