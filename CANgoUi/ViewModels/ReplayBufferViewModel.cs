﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using CANFrameLib;
using CANgoUi.Mappers;
using CANgoUi.Models.DataTypes;
using CANgoUi.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using LawicelLib;

namespace CANgoUi.ViewModels
{
    public class ReplayBufferViewModel : ViewModelBase
    {
        // Public
        public ObservableCollection<CANFrame> ReplayBuffer { get; set; }
        
        public int Maximum { get; set; }
        public bool IsRunning { get; set; } = false;
        public bool Looped { get; set; } = false;
        public int CurrentItem { get; set; }
        public int LowerValue { get; set; }
        public int HigherValue { get; set; }

        // Private
        private readonly FrameBufferService _frameBufferService;
        private readonly TransmitService _transmitService;
        private readonly ConnectionService _connectionService;
        private readonly MessageService _messageService;
        private readonly Lawicel _lawicel;
        private BackgroundWorker _worker;

        // Commands
        public RelayCommand RunCmd => new RelayCommand(Run);
        public RelayCommand StepOneCmd => new RelayCommand(StepOne);
        public RelayCommand StopRunCmd => new RelayCommand(StopRun);
        public RelayCommand ResetCmd => new RelayCommand(Reset);

        public ReplayBufferViewModel(FrameBufferService frameBufferService, TransmitService transmitService,
            ConnectionService connectionService, MessageService messageService, Lawicel lawicel)
        {
            _frameBufferService = frameBufferService;
            _transmitService = transmitService;
            _connectionService = connectionService;
            _messageService = messageService;
            _lawicel = lawicel;

            ReplayBuffer = new ObservableCollection<CANFrame>(frameBufferService.CANFrames);

            Maximum = ReplayBuffer.Count;
            LowerValue = 0;
            HigherValue = ReplayBuffer.Count;
            CurrentItem = LowerValue;
        }

        /// <summary>
        /// Run buffer replay
        /// </summary>
        public async void Run()
        {
            if (_connectionService.ConnectionStatus == ConnectionStatus.ONLINE)
            {
                _worker = new BackgroundWorker();
                _worker.WorkerSupportsCancellation = true;
                _worker.DoWork += Worker_DoWorkAsync;
                _worker.RunWorkerAsync();
            }
            else
            {
                if (_messageService.ShowDialog("You must connect to device.\n\r Connect?") == MessageBoxResult.Yes)
                {
                    await _connectionService.ConnectAsync();
                }
            }
        }

        private async void Worker_DoWorkAsync(object sender, DoWorkEventArgs e)
        {
            _transmitService.StopAllFrames();

            IsRunning = true;
            if (CurrentItem < LowerValue)
            {
                CurrentItem = LowerValue;
            }

            if (CurrentItem > HigherValue)
            {
                CurrentItem = HigherValue;
            }

            for (CurrentItem = CurrentItem; CurrentItem < HigherValue - 1; CurrentItem++)
            {
                var millisecondsDelay = (int)(ReplayBuffer[CurrentItem + 1].SystemTimeStamp -
                                               ReplayBuffer[CurrentItem].SystemTimeStamp);

                await _lawicel.SendTxFrame(ReplayBuffer[CurrentItem].MapToByteArray());
                Thread.Sleep(millisecondsDelay);

                if (!IsRunning)
                {
                    break;
                }
            }

            if (CurrentItem ==  HigherValue - 1 )
            {
                IsRunning = false;
                CurrentItem = LowerValue;
                if (Looped)
                {
                    Run();
                }
            }
        }

        /// <summary>
        /// Stop buffer replay
        /// </summary>
        public void StopRun()
        {
            IsRunning = false;
        }

        /// <summary>
        /// Reset current index
        /// </summary>
        public void Reset()
        {
            CurrentItem = LowerValue;
        }

        /// <summary>
        /// Replay by one step
        /// </summary>
        public async void StepOne()
        {
            if (_connectionService.ConnectionStatus == ConnectionStatus.ONLINE)
            {
                if (CurrentItem< HigherValue)
                {
                    await _lawicel.SendTxFrame(ReplayBuffer[CurrentItem].MapToByteArray());
                    CurrentItem++;
                }
            }
            else
            {
                if (_messageService.ShowDialog("You must connect to device.\n\r Connect?") == MessageBoxResult.Yes)
                {
                    await _connectionService.ConnectAsync();
                }
            }
        }
    }
}