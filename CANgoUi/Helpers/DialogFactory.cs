﻿using Microsoft.Win32;

namespace CANgoUi.Helpers
{
    public static class DialogFactory
    {
        public static OpenFileDialog GetFileOpenDialog(string filter = "")
        {
            var openDialog = new OpenFileDialog();
            openDialog.CheckFileExists = true;
            openDialog.CheckPathExists = true;
            openDialog.RestoreDirectory = true;
            openDialog.ReadOnlyChecked = true;
            openDialog.ShowReadOnly = true;
            openDialog.FileName = "";
            openDialog.Filter = filter;
            //openDialog.Filter = "Text file (*.chunk)|*.chunk";
            return openDialog;
        }

        public static SaveFileDialog GetSaveFileDialog(string filter = "")
        {
            var saveDialog = new SaveFileDialog();
            saveDialog.Filter = filter;

            return saveDialog;
        }
    }
}