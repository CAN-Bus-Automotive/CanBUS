﻿using System.Collections.ObjectModel;
using CANFrameLib;
using CANgoUi.Models.DataTypes.Dbf.Impl;

namespace CANgoUi.Models.DataTypes.Dbf
{
    public interface IDbfMessage
    {
        uint Id { get; }
        int Dcl { get; set; }
        string Description { get; set; }
        ObservableCollection<Signal> Signals { get; set; }
        void UpdateSignals(CANFrame canFrame);
    }
}