﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Xml.Serialization;
using CANgoUi.Models.DataTypes;

namespace CANgoUi.Services
{
    public class UserSettingsService
    {
        public readonly string DefaultFilepath;

        private readonly string _defaultFileName;
        private readonly string _defaultFileDirectory;

        public UserSettingsService()
        {
            _defaultFileDirectory = AppDomain.CurrentDomain.BaseDirectory;
            _defaultFileName = $"cango_v{Assembly.GetExecutingAssembly().GetName().Version}";
            DefaultFilepath = $"{_defaultFileDirectory}{_defaultFileName}.uconf";
        }

        /// <summary>
        /// Load settings from file
        /// </summary>
        /// <returns>UserSettings</returns>
        public UserSettings LoadAsync()
        {
            UserSettings result = null;

            using (StreamReader file = File.OpenText(DefaultFilepath))
            {
                var serializer = new XmlSerializer(typeof(UserSettings));
                result = (UserSettings) serializer.Deserialize(file);
            }

            return result;
        }

        /// <summary>
        /// Save user settings to default settings path
        /// </summary>
        /// <param name="settings"></param>
        public void Save(UserSettings settings)
        {
            using (StreamWriter file = File.CreateText(DefaultFilepath))
            {
                try
                {
                    var serializer = new XmlSerializer(typeof(UserSettings));
                    serializer.Serialize(file, settings);
                }
                catch (Exception e)
                {
                    Debug.Fail(e.Message);
                }
            }
        }

        /// <summary>
        /// Get user settings with default values
        /// </summary>
        /// <returns>UserSettings</returns>
        public UserSettings GetDefaultSettings()
        {
            return new UserSettings();
        }
    }

}