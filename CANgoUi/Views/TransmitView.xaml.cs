﻿using System.Globalization;
using System.Windows.Controls;
using System.Windows.Input;

namespace CANgoUi.Views
{
    /// <summary>
    /// Interaction logic for TransmitVIew.xaml
    /// </summary>
    public partial class TransmitView : UserControl
    {
        public TransmitView()
        {
            InitializeComponent();
        }

        private void OnPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int hexNumber;
            e.Handled = !int.TryParse(e.Text, NumberStyles.HexNumber, CultureInfo.CurrentCulture, out hexNumber);
        }
    }
}
