﻿namespace CANgoUi.Models.DataTypes
{
    public enum ConnectionStatus
    {
        OFFLINE = 0,
        ONLINE
    }
}
