﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Windows;
using CANgoUi.Models.DataTypes;
using CANgoUi.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using NLog;

namespace CANgoUi.ViewModels
{
    public class ShellViewModel : ViewModelBase
    {
        // Utility
        private readonly Logger _log = NLog.LogManager.GetCurrentClassLogger();

        // Private
        private readonly UserSettingsService _settingsService;
        private readonly ConnectionService _connectionService;
        private readonly MessageService _messageService;
        private readonly FrameBufferService _frameBufferService;
        private UserSettings _userSettings;

        // Public
        public string BuildVersion { get; private set; } =
            "CANgo v." + FileVersionInfo.GetVersionInfo
                               (Assembly.GetExecutingAssembly().Location).ProductVersion;

        public WindowState WindowState { get; set; }
        public int TopPosition { get; set; }
        public int LeftPosition { get; set; }
        public int WindowWith { get; set; }
        public int WindowHeight { get; set; }

        // Commands
        public RelayCommand CloseShellWindowCmd => new RelayCommand(CloseShellWindow);
        public RelayCommand LoadShellWindowCmd => new RelayCommand(LoadShellWindow);


        // Constructor
        public ShellViewModel(UserSettingsService settingsService, ConnectionService connectionService,
            MessageService messageService, FrameBufferService frameBufferService)
        {
            _settingsService = settingsService;
            _connectionService = connectionService;
            _messageService = messageService;
            _frameBufferService = frameBufferService;

            ReadSettings();
        }


        /// <summary>
        /// Close shell window handler
        /// </summary>
        public void CloseShellWindow()
        {
            // Disconnect from device
            _connectionService.Disconnect();

            SaveSettings();
        }

        public void LoadShellWindow()
        {
            // TODO Add some actions))
        }

        /// <summary>
        /// Read settings from file
        /// </summary>
        public void ReadSettings()
        {
            try
            {
                _userSettings = _settingsService.LoadAsync();
            }
            catch (Exception e)
            {
                _log.Error(e);
                //_messageService.ShowError(e.Message);
                _userSettings = _settingsService.GetDefaultSettings();
            }
            finally
            {
                // Window setup
                WindowHeight = _userSettings.WindowHeight;
                WindowWith = _userSettings.WindowWith;
                LeftPosition = _userSettings.LeftPosition;
                TopPosition = _userSettings.TopPosition;
                WindowState = _userSettings.WindowState;
                //SetupVm.ShowBitView = UserSettings.ShowBitView;

                // Connection setup
                _connectionService.SelectedPort = _userSettings.PortName;
                _connectionService.SelectedBaud = _userSettings.Baudrate;

                // Buffer setup
                _frameBufferService.IsBitViewVisibility = _userSettings.BitViewVisibility;
            }
        }

        public void SaveSettings()
        {
            // Window setup
            _userSettings.WindowHeight = WindowHeight;
            _userSettings.WindowWith = WindowWith;
            _userSettings.LeftPosition = LeftPosition;
            _userSettings.TopPosition = TopPosition;
            _userSettings.WindowState = WindowState;
            //_userSettings.ShowBitView = SetupVm.ShowBitView;

            // Connection setup
            _userSettings.PortName = _connectionService.SelectedPort;
            _userSettings.Baudrate = _connectionService.SelectedBaud;
            
            // Buffer setup
            _userSettings.BitViewVisibility = _frameBufferService.IsBitViewVisibility;

            _settingsService.Save(_userSettings);
        }
    }
}