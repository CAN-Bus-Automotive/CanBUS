﻿using System.Threading.Tasks;
using System.Windows;
using CANgoUi.Models.DataTypes;
using CANgoUi.Services;
using CommonServiceLocator;
using PostSharp.Aspects;
using PostSharp.Serialization;

namespace CANgoUi.Helpers
{
    [PSerializable]
    public class CheckConnectionAttribute : MethodInterceptionAspect
    {
        [PNonSerialized] private ConnectionService _connectionService;
        [PNonSerialized] private MessageService _messageService;

        public override async void OnInvoke(MethodInterceptionArgs args)
        {
            ViewModelLocator.Initialize();
            _connectionService = ServiceLocator.Current.GetInstance<ConnectionService>();
            _messageService = ServiceLocator.Current.GetInstance<MessageService>();

            if (_connectionService.ConnectionStatus == ConnectionStatus.ONLINE)
            {
                base.OnInvoke(args);
            }
            else
            {
                if (_messageService.ShowDialog("You must connect to device.\n\r Connect?") == MessageBoxResult.Yes)
                {
                    await _connectionService.ConnectAsync();
                }
            }
        }
    }
}