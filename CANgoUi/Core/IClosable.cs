﻿namespace CANgoUi.Core
{
    public interface IClosable
    {
        void Close();
    }
}
